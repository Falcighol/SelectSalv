<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<!-- Bootstrap css -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/style.css">
	<!-- Icono de la página -->
	<link rel="icon" type="image/ico" href="img/icon.ico">
	<!-- Scripts -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.js"></script>
    <script src="js/fontawesome-all.js"></script>
	<script src="js/loader.js"></script>
	<script src="js/charts.js"></script>
	<title>Estadísticas</title>
</head>
<body>
	<!-- Menu -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<!-- Brand -->
		<a href="index.php" class="navbar-brand" style="margin: auto 1rem auto 0;">
			<img src="img/email-icon.png" width="30" class="d-inline-block align-top" alt="SelectSalv">
			SelectSalv
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent" style="text-align: center;">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="index.php"><span class="fas fa-home"></span> Inicio</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="charts.php"><span class="fas fa-chart-line"></span> Estadísticas</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="contact.php"><span class="fas fa-phone"></span> Contáctanos</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="about.php"><span class="fas fa-info"></span> Sobre nosotros</a>
				</li>
			</ul>
			<div class="form-inline my-2 my-lg-0" align='center'>
				<a class="btn btn-outline-light my-2 my-sm-0" href="login/" style="margin: auto;"><i class="fas fa-sign-in-alt"></i> Votar / Iniciar Sesión</a>
			</div>
		</div>
	</nav>
	<div class="container">
        <div class="jumbotron" style="margin-top: 3rem; background-color: #fff;">
            <h2 class="font-weight-light">Estadísticas de votos</h2>
			<hr class="my-4">
			<div class="table-responsive-md">
				<div id="chartPar" style="height: 30rem; margin: 2rem auto;"></div>
				<hr>
				<div id="chartDep" style="height: 30rem; margin: 2rem auto;"></div>
			</div>
        </div>
		<footer style="padding: 1rem;">
			<p style="text-align: center;">
                15 Calle poniente No. 4223, Colonia Escalón
                <br>
                San Salvador, El Salvador, C.A.
                <br>
                Conmutador: (503) 2209-4000
			</p>
            <div style="margin: 1rem auto;" align="center">
                <img src="img/escudo.png" alt="Escudo" style="width: 5rem;">
            </div>
            <p style="text-align: center;">Selectsalv &copy; <?php echo date('Y'); ?> Todos los derechos reservados.</p>
		</footer>
	</div>
</body>
</html>