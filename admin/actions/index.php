<?php
    // Redireccionador
    session_start();
    if (isset($_SESSION['usuario']))
    {
        if ($_SESSION['usuario']['tipo_Usuario'] == 2) {
            header('Location: ../../votar/');
            exit;
        }
        else
        {
            header('Location: ../');
            exit;
        }
    }
    else
    {
        header('Location: ../../login/');
        exit;
    }
?>