<?php
    session_start();
    if (isset($_SESSION['usuario']))
    {
        if ($_SESSION['usuario']['tipo_Usuario'] == 2)
        {
            header('Location: ../../../votar/');
            exit;
        }
    }
    else
    {
        header('Location: ../../../login/');
        exit;
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<!-- Bootstrap css -->
	<link rel="stylesheet" href="../../../css/bootstrap.css">
	<link rel="stylesheet" href="../../../css/style.css">
	<!-- Icono de la página -->
	<link rel="icon" type="image/ico" href="../../../img/icon.ico">
	<!-- Scripts -->
	<script src="../../../js/jquery-3.2.1.min.js"></script>
	<script src="../../../js/popper.min.js"></script>
	<script src="../../../js/bootstrap.js"></script>
	<script src="../../../js/fontawesome-all.js"></script>
	<script src="../../../js/jrv.js"></script>
	<title>Juntas receptoras de votos</title>
</head>
<body style="background-color: #fff;">
	<!-- Menu -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<h3 class="navbar-brand" style="margin: auto 1rem auto 0;">
			<img src="../../../img/email-icon.png" width="30" height="30" class="d-inline-block align-top" alt="SelectSalv">
			SelectSalv
		</h3>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent" style="text-align: center;">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="../../"><span class="fas fa-home"></span> Inicio</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../charts/"><span class="fas fa-chart-line"></span> Estadísticas</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../usuarios/"><span class="fas fa-user-circle"></span> Usuarios</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../candidatos/"><span class="fas fa-users"></span> Candidatos</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../partidos/"><span class="fas fa-flag"></span> Partidos</a>
				</li>
				<li class="nav-item dropdown active">
					<a class="nav-link dropdown-toggle" href="" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Más
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="../cv/"><span class="fas fa-map-marker-alt"></span> Centros de votación</a>
						<a class="dropdown-item active" href="index.php"><span class="fas fa-archive"></span> Juntas receptoras</a>
						<a class="dropdown-item" href="../votar/"><span class="fas fa-address-card"></span> Votar</a>
					</div>
				</li>
			</ul>
			<form class="form-inline my-2 my-lg-0" align='center' action="../../../php/Logout.php">
				<button class="btn btn-outline-light my-2 my-sm-0" type="submit" style="margin: auto;"><li class="fas fa-sign-out-alt"></li> Cerrar sessión</button>
			</form>
		</div>
	</nav>
	<!-- Formulario Modal Agregar Junta Receptora de Votos-->
	<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="Agregar" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Nueva Junta Receptora de Votos</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="frmAgregar">
						<div class="form-group">
							<label for="txtNumero" class="col-form-label">Numero de JRV:</label>
							<input type="text" class="form-control addRequired" id="txtNumero" required>
						</div>
						<div class="form-group">
							<label for="cmbCV" class="col-form-label">Centro de votación al que pertenece:</label>
							<select class="form-control addRequired" id="cmbCV">
								<!-- Contenido de Select-->
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button id="btnAgregar" type="button" class="btn btn-primary"><li class="fas fa-save"></li> Guardar</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Formulario Modal Editar Junta Receptora de Votos-->
	<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="Editar" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Editar Junta Receptora de Votos</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="frmEditar">
						<div class="form-group" style="display: none;">
							<label for="txtEditarId" class="col-form-label">ID:</label>
							<input type="text" class="form-control editRequired disabled" id="txtEditarId" name="txtEditarId" required disabled="true">
						</div>
						<div class="form-group">
							<label for="txtEditarNumero" class="col-form-label">Número del centro de votación:</label>
							<input type="text" class="form-control editRequired" id="txtEditarNumero" name="txtEditarNumero" required>
						</div>
						<div class="form-group">
							<label for="cmbEditarCV" class="col-form-label">Centro de votación al que pertenece:</label>
							<select class="form-control editRequired" id="cmbEditarCV" name="txtEditarCV" required>
								<!--Opciones-->
							</select>
							<br>
							<span style="color: #7A7A7AFF;" id="nombreCV" class="small"></span>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button id="btnEditar" type="button" class="btn btn-primary"><li class="fas fa-save"></li> Guardar</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Formulario Modal Confirmar Eliminar -->
	<div class="modal fade" id="del" tabindex="-1" role="dialog" aria-labelledby="Eliminar" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Eliminar Junta Receptora de Votos</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h6 id="pregunta"> ¿Desea eliminar el registro x? </h6>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
					<button id="btnEliminar" type="button" class="btn btn-primary"><li class="fas fa-trash-alt"></li> Eliminar</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Contenedor -->
	<div class="container" style="background-color: #fff;">
		<div class="jumbotron jumbotron-fluid" style="margin: 1rem auto; background-color: #fff; padding: 1rem 0;">
			<div class="container">
				<h1 class="display-4">Gestionar juntas receptoras de votos.</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-md-6" style="margin: 0 auto 1rem auto;">
				<input type="text" id="txtBuscar" class="form-control" width="100%" placeholder="Buscar">
			</div>
			<div class="col-sm-12 col-md-3" style="margin: 0 auto 1rem auto;">
				<button class="btn btn-primary" id="btnNuevo" style="width: 100%;" data-toggle="modal" data-target="#add"><li class="fas fa-plus"></li> Nuevo</button>
			</div>
			<div class="col-sm-12 col-md-3" style="margin: 0 auto 1rem auto;">
				<button class="btn btn-primary" style="width: 100%;"><li class="fas fa-file-pdf"></li> Reportes</button>
			</div>
		</div>
		<style type="text/css">
			.alert-success { display: none; }
			.alert-danger { display: none; }
		</style>
		<div class="alert alert-success" role="alert">
			Consulta ejecutada correctamente
		</div>
		<div class="alert alert-danger" role="alert">
			Error al ejecutar la consulta
		</div>
		<div class="table-responsive">
			<table id="tablaDatos" class="table table-hover table-bordered" style="display: none; text-align: center;">
				<thead class="thead-light">
					<tr>
						<!--<th scope="col">ID</th>-->
						<th scope="col" class="align-middle">Numero de JRV</th>
						<th scope="col" class="align-middle">Nombre de centro de votación</th>
						<th scope="col" class="align-middle">Acciones</th>
					</tr>
				</thead>
				<tbody id="cuerpoTablaDatos">
					<!-- Contenido de tabla-->
				</tbody>
			</table>
		</div>
	</div>
	<footer style="padding: 1rem;">
		<p style="text-align: center;">
			15 Calle poniente No. 4223, Colonia Escalón
			<br>
			San Salvador, El Salvador, C.A.
			<br>
			Conmutador: (503) 2209-4000
		</p>
		<div style="margin: 1rem auto;" align="center">
			<img src="../../../img/escudo.png" alt="Escudo" style="width: 5rem;">
		</div>
		<p style="text-align: center;">Selectsalv &copy; <?php echo date('Y'); ?> Todos los derechos reservados.</p>
	</footer>
</body>
</html>