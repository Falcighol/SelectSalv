<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<!-- Bootstrap css -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/style.css">
	<!-- Icono de la página -->
	<link rel="icon" type="image/ico" href="img/icon.ico">
	<!-- Scripts -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/fontawesome-all.js"></script>
	<title>SelectSalv</title>
</head>
<body>
	<!-- Menu -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<!-- Brand -->
		<a href="index.php" class="navbar-brand" style="margin: auto 1rem auto 0;">
			<img src="img/email-icon.png" width="30" class="d-inline-block align-top" alt="SelectSalv">
			SelectSalv
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent" style="text-align: center;">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active">
					<a class="nav-link" href="index.php"><span class="fas fa-home"></span> Inicio</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="charts.php"><span class="fas fa-chart-line"></span> Estadísticas</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="contact.php"><span class="fas fa-phone"></span> Contáctanos</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="about.php"><span class="fas fa-info"></span> Sobre nosotros</a>
				</li>
			</ul>
			<div class="form-inline my-2 my-lg-0" align='center'>
				<a class="btn btn-outline-light my-2 my-sm-0" href="login/" style="margin: auto;"><i class="fas fa-sign-in-alt"></i> Votar / Iniciar Sesión</a>
			</div>
		</div>
	</nav>
	<div class="container">
        <div class="jumbotron bg-light text-center" style="margin-top: 3rem;">
            <h1 id="tit" class="">SELECTSALV</h1>
            <p class="lead">Bienvenido a SelectSalv, la página de votación en línea para las elecciones presidenciales 2019.</p>
            <hr class="my-4">
			<blockquote class="blockquote text-center">
				<p class="mb-0 font-italic">"Una nación sin elecciones libres es una nación sin voz, sin ojos y sin brazos."</p>
				<footer class="blockquote-footer"><cite title="Source Title">Octavio Paz</cite></footer>
			</blockquote>
            <p class="lead">
                <a class="btn btn-outline-primary btn-lg" href="login/" role="button">Comenzar <i class="fas fa-sign-in-alt" aria-hidden="true"></i></a>
            </p>
        </div>
		<div class="jumbotron bg-light" style="margin-top: 3rem;">
            <h2 class="font-weight-light">Ver estadisticas de votos</h2>
            <p class="lead">Vea estadísticas de los votos en tiempo real por departamentos, por candidato, etc.</p>
            <hr class="my-4">
            <p class="lead">
                <a class="btn btn-outline-primary btn-lg" href="charts.php" role="button">Ver estadísticas<i class="fas fa-sign-in-alt" aria-hidden="true"></i></a>
            </p>
        </div>
		<div class="jumbotron bg-light" style="margin-top: 3rem;">
            <h2 class="font-weight-light">Institución</h2>
			<p class="lead">Directores de vigilancia electoal</p>
			<div class="table-responsive-md">
				<table class="table">
					<thead>
						<tr>
							<th scope="col">Partido</th>
							<th scope="col">Director Propietario</th>
							<th scope="col">Director Suplente</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><strong>ARENA</strong></td>
							<td>Lic. Selim Ernesto Alab&iacute; Mendoza</td>
							<td>
							<p>Lic. Luis Eduardo Qui&ntilde;onez Alas</p>
							</td>
						</tr>
						<tr>
							<td><strong>FMLN</strong></td>
							<td>Arq. Ana Gulnara Marroqu&iacute;n</td>
							<td>
							<p>Lic. Jos&eacute; Dennis C&oacute;rdova Elizondo</p>
							</td>
						</tr>
						<tr>
							<td><strong>PDC</strong></td>
							<td>Ing. Edwin Patricio N&uacute;&ntilde;ez</td>
							<td>
							<p>Lic. Bayron Alejandro Chachagua</p>
							</td>
						</tr>
						<tr>
							<td><strong>CD</strong></td>
							<td>Lic. Juan Jos&eacute; Martel</td>
							<td>
							<p>Lic. Aldonov Frankeko &Aacute;lvarez</p>
							</td>
						</tr>
						<tr>
							<td><strong>GANA</strong></td>
							<td>Licda. Silvia Aguilar</td>
							<td>
							<p>Lic. Erick Ernesto Campos</p>
							</td>
						</tr>
						<tr>
							<td><strong>FPS</strong></td>
							<td>Lic. Jos&eacute; Oscar Morales Lemus</td>
							<td>
							<p>Lic. Rey Adalberto Agui&ntilde;ada Cruz</p>
							</td>
						</tr>
						<tr>
							<td><strong>PCN</strong></td>
							<td>Lic. Ciro Alexis Zepeda Menj&iacute;var</td>
							<td>
							<p>Lic. Manuel Alberto Rodr&iacute;guez Molina</p>
							</td>
						</tr>
						<tr>
							<td><strong>PSP</strong></td>
							<td>Cap. Rodolfo Armando P&eacute;rez Valladares</td>
							<td>
							<p>Lic. Jos&eacute; Ren&aacute;n Orantes Jovel</p>
							</td>
						</tr>
						<tr>
							<td><strong>DS</strong></td>
							<td>Lic. Ezequiel Mendoza Ferm&aacute;n</td>
							<td>
							<p>Lic. Rigoberto Antonio Ortiz</p>
							</td>
						</tr>
						<tr>
							<td><strong>PSD</strong></td>
							<td>Sr. Carlos Romualdo Marroq&iacute;n</td>
							<td>
							<p>Sr. Ra&uacute;l Edwin Rojas Rodr&iacute;guez</p>
							</td>
						</tr>
					</tbody>
					<tfoot>
						<tr>
							<th>Director Ejecutivo</th>
							<td colspan='2'>Lic. Jos&eacute; Antonio Palacios</td>
						</tr>
					</tfoot>
				</table>
			</div>
        </div>
		<footer style="padding: 1rem;">
			<p style="text-align: center;">
                15 Calle poniente No. 4223, Colonia Escalón
                <br>
                San Salvador, El Salvador, C.A.
                <br>
                Conmutador: (503) 2209-4000
			</p>
            <div style="margin: 1rem auto;" align="center">
                <img src="img/escudo.png" alt="Escudo" style="width: 5rem;">
            </div>
            <p style="text-align: center;">Selectsalv &copy; <?php echo date('Y'); ?> Todos los derechos reservados.</p>
		</footer>
	</div>
</body>
</html>