<?php
	require_once('CRUD.php');
	require_once('../ds/DataSource.php');
	require_once('../dto/Usuario.php');

	class UsuarioDAO implements CRUD
	{
	    function mostrar()
	    {
	        // Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				// Enviando error de conexión
				echo 'Error al conectar: '.$conn->conectar()->connect_error;
				exit;
			}
			else
			{
				$sql = 'CALL mostrarUsuarios()';
				if ($stmt = $conn->preparar($sql))
				{
					// Ejecutando la consulta
					$stmt->execute();
					// Asignando a la variable el resultado de la consulta 
					$resultado = $stmt->get_result();
					if ($resultado->num_rows > 0) 
					{
						// Enviando valores obtenidos de la consulta
						foreach ($resultado as $indice => $valor) 
						{
							echo "<tr>
							<td class='align-middle duiPersona'>".$valor['DUI_Persona']."</td>
							<td class='align-middle nombrePersona'>".$valor['Nombre_Persona']." ".$valor['Apellido_Persona']."</td>
							<td class='align-middle TU'>".$valor['Descripcion_TU']."</td>
							<td class='align-middle EVU'>".$valor['Descripcion_EVU']."</td>
							<td class='align-middle' id='".$valor['Id_Usuario']."'>
								<button class='btn btn-info btn-sm editar' data-toggle='modal' data-target='#edit'><li class='fas fa-edit'></li> Editar</button>
								<button class='btn btn-danger btn-sm eliminar' data-toggle='modal' data-target='#del'><li class='fas fa-trash-alt'></li> Eliminar</button>
							</td>
							</tr>";
						}
						// Cerrando conexión
						$stmt->close();
						$conn->desconectar();
					}
					else 
					{
						// Enviando respuesta si no hay datos
						echo "<tr><td class='align-middle' colspan='5'>No hay datos.</td></tr>";
					}
				}
				else
				{
					// Cerrando conexión
					$stmt->close();
					$conn->desconectar();
					// Información del error
					echo 'Error al realizar la consulta: '.$conn->error();
					exit;
				}
			}
	    } // Fin de función mostrar

	    function agregar($obj)
	    {
	        // Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				// Enviando error de conexión
				echo json_encode(array('error' => true, 'info' => $conn->error()));
				exit;
			}
			else
			{
				$usuario = $obj;
				$sql = "CALL agregarUsuario(?, ?, ?)";
				// Verificando si la consulta se realizó con exito o no
				if ($stmt = $conn->preparar($sql)) 
				{
					// Enviando parámetros a la consulta
					$stmt->bind_param("sii", $pass, $idTU, $idPersona);
					$pass = password_hash($usuario->passUsuario, PASSWORD_DEFAULT);
					$idTU = $usuario->idTU;
					$idPersona = $usuario->idPersona;
					// Ejecutando consulta
					$stmt->execute();
					// Enviando respuesta
					echo json_encode(array('error' => false, 'info' => 'Registro guardado correctamente'));
				} 
				else 
				{
					// Enviando respuesta
					echo json_encode(array('error' => true, 'info' => $conn->error()));
					exit;
				}
				// Cerrando conexión
				$stmt->close();
				$conn->desconectar();
			}
	    } // Fin de función agregar

	    function modificar($obj)
	    {
	        // Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				echo json_encode(array('error' => true, 'info' => $conn->conectar()->connect_error));
				exit;
			}
			else
			{
				$usuario = $obj;
				$sql = "CALL editarUsuario(?, ?, ?, ?)";
				// Verificando si la consulta se realizó correctamente
				if ($stmt = $conn->preparar($sql))
				{
					// Enviando parámetros a la consulta
					$stmt->bind_param('iiis', $idUsuario, $idTU, $idEVU, $pass);
					$idUsuario = $usuario->idUsuario;
					$idTU = $usuario->idTU;
					$idEVU = $usuario->idEVU;
					$pass = password_hash($usuario->passUsuario, PASSWORD_DEFAULT);
					// Ejecutando consulta
					$stmt->execute();
					// Enviando respuesta
					echo json_encode(array('error' => false, 'info' => 'Registro editado correctamente'));
				}
				else
				{
					// Enviando respuesta
					echo json_encode(array('error' => true, 'info' => $conn->error()));
					$stmt->close();
				}
				// Cerrando conexión
				$conn->desconectar();
				exit;
			}
	    } // Fin de función modificar

	    function eliminar($obj)
	    {
	        // Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				echo json_encode(array('error' => true, 'info' => $conn->conectar()->connect_error));
				exit;
			}
			else
			{
				$usuario = $obj;
				$sql = "CALL eliminarUsuario(?)";
				// Verificando si la consulta se realizó correctamente
				if ($stmt = $conn->preparar($sql))
				{
					// Enviando parámetros
					$stmt->bind_param('i', $idUsuario);
					$idUsuario = $usuario->idUsuario;
					// Ejecutando consulta
					$stmt->execute();
					// Enviando respuesta
					echo json_encode(array('error' => false, 'info' => 'Registro eliminado correctamente'));
				}
				else
				{
					// Enviando respuesta
					echo json_encode(array('error' => true, 'info' => $conn->error()));
				}
				// Cerrando conexión
				$stmt->close();
				$conn->desconectar();
			}
	    } // Fin de función eliminar

	    function buscar($val)
	    {
	        // Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				echo 'Error al conectar: '.$conn->conectar()->connect_error;
				exit;
			}
			else
			{
				$sql = "SELECT U.Id_Usuario, TU.Descripcion_TU, EVU.Descripcion_EVU, P.Nombre_Persona, P.DUI_Persona, P.Apellido_Persona FROM Usuario U INNER JOIN Persona P ON U.Id_Persona = P.Id_Persona INNER JOIN Tipo_Usuario TU ON U.Id_TU = TU.Id_TU INNER JOIN Estado_Voto_Usuario EVU ON U.Id_EVU = EVU.Id_EVU WHERE P.DUI_Persona LIKE '%$val%'OR P.Nombre_Persona LIKE '%$val%' LIMIT 10";
				if ($stmt = $conn->preparar($sql))
				{
					// Ejecutando la consulta
					$stmt->execute();
					// Obteniendo el resultado de la consulta
					$resultado = $stmt->get_result();
					// Verificando si la consulta devuelve datos
					if ($resultado->num_rows > 0) 
					{
						// Enviando valores obtenidos de la consulta
						foreach ($resultado as $indice => $valor) 
						{
							echo "<tr>
							<td class='align-middle duiPersona'>".$valor['DUI_Persona']."</td>
							<td class='align-middle nombrePersona'>".$valor['Nombre_Persona']." ".$valor['Apellido_Persona']."</td>
							<td class='align-middle TU'>".$valor['Descripcion_TU']."</td>
							<td class='align-middle EVU'>".$valor['Descripcion_EVU']."</td>
							<td class='align-middle' id='".$valor['Id_Usuario']."'>
								<button class='btn btn-info btn-sm editar' data-toggle='modal' data-target='#edit'><li class='fas fa-edit'></li> Editar</button>
								<button class='btn btn-danger btn-sm eliminar' data-toggle='modal' data-target='#del'><li class='fas fa-trash-alt'></li> Eliminar</button>
							</td>
							</tr>";
						}
					}
					else 
					{
						echo "<tr><td class='align-middle' colspan='5'>No hay coincidencias para {$val}.</td></tr>";
					}
				}
				else
				{
					echo 'Error al realizar la consulta: '.$conn->error();
					exit;
				}
				$stmt->close();
				$conn->desconectar();
			}
	    } // Fin de función buscar

	    function buscarPersona($val)
		{
			// Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				// Enviando error de conexión
				echo 'Error al conectar: '.$conn->conectar()->connect_error;
				exit;
			}
			else
			{
				$valor = $val;
				$sql = 'CALL seleccionarPersona(?)';
				if ($stmt = $conn->preparar($sql))
				{
					// Ejecutando la consulta
					$stmt->bind_param('s', $valor);
					$stmt->execute();
					// Asignando a la variable el resultado de la consulta 
					$resultado = $stmt->get_result();
					// Verificando si hay datos
					if ($resultado->num_rows == 1) 
					{
						// Convirtiendo a un arreglo asociativo
						$datos = $resultado->fetch_assoc();
						// Enviando respuesta
						echo json_encode(array('error' => false, 'id' => $datos['Id_Persona'], 'nombre' => $datos['Nombre_Persona'].' '.$datos['Apellido_Persona']));
					}
					else
					{
						echo json_encode(array('error' => true, 'info' => 'El DUI es incorrecto.'));
					}
					// Cerrando conexión
					$stmt->close();
					$conn->desconectar();
				}
				else
				{
					// Cerrando conexión
					$stmt->close();
					$conn->desconectar();
					// Información del error
					echo json_encode(array('error' => true, 'info' => 'Error al realizar la consulta: '.$conn->error()));
					exit;
				} // Fin de validación de consulta
			} // Fin de validación de conexión
		} // Fin de función buscar persona

	    function llenarSelect1()
	    {
	    	// Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				// Enviando error de conexión
				echo 'Error al conectar: '.$conn->conectar()->connect_error;
				exit;
			}
			else
			{
				$sql = 'SELECT Id_TU, Descripcion_TU FROM tipo_usuario';
				if ($stmt = $conn->preparar($sql))
				{
					// Ejecutando la consulta
					$stmt->execute();
					// Asignando a la variable el resultado de la consulta 
					$resultado = $stmt->get_result();
					// Enviando respuesta
					echo '<option value="0">-Seleccionar-</option>';
					// Verificando si hay datos
					if ($resultado->num_rows > 0) 
					{
						foreach ($resultado as $indice => $valor) 
						{
							echo '<option value="'.$valor['Id_TU'].'">'.$valor['Descripcion_TU'].'</option>';
						}
					}
					// Cerrando conexión
					$stmt->close();
					$conn->desconectar();
				}
				else
				{
					// Cerrando conexión
					$stmt->close();
					$conn->desconectar();
					// Información del error
					echo 'Error al realizar la consulta: '.$conn->error();
					exit;
				}
			} // Fin de validación de coneión
	    } // Fin de llenarSelect1

		function llenarSelect2()
		{
			// Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				// Enviando error de conexión
				echo 'Error al conectar: '.$conn->conectar()->connect_error;
				exit;
			}
			else
			{
				$sql = 'SELECT Id_EVU, Descripcion_EVU FROM estado_voto_usuario';
				if ($stmt = $conn->preparar($sql))
				{
					// Ejecutando la consulta
					$stmt->execute();
					// Asignando a la variable el resultado de la consulta 
					$resultado = $stmt->get_result();
					// Enviando respuesta
					echo '<option value="0">-Seleccionar-</option>';
					// Verificando si hay datos
					if ($resultado->num_rows > 0) 
					{
						foreach ($resultado as $indice => $valor) 
						{
							echo '<option value="'.$valor['Id_EVU'].'">'.$valor['Descripcion_EVU'].'</option>';
						}
					}
					// Cerrando conexión
					$stmt->close();
					$conn->desconectar();
				}
				else
				{
					// Cerrando conexión
					$stmt->close();
					$conn->desconectar();
					// Información del error
					echo 'Error al realizar la consulta: '.$conn->error();
					exit;
				} // Fin de validación de consulta
			} // Fin de validación de conexión
		} // Fin de llenarSelect2

		function verificar($obj)
		{
			// Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				echo json_encode(array('error' => true, 'info' => $conn->conectar()->connect_error));
				exit;
			}
			else
			{
				$usuario = $obj;
				$sql = 'CALL verificarUsuario(?)';
				if ($stmt = $conn->preparar($sql))
				{
					$stmt->bind_param('s', $duiPersona);
					$duiPersona = $usuario->duiPersona;
					$stmt->execute();
					$resultado = $stmt->get_result();
					if ($resultado->num_rows == 1)
					{
						// Convirtiendo los datos de la consulta a un arreglo asosiativo e igualándolo a la variable Datos 
						$datos = $resultado->fetch_assoc();
						session_start();
						// if ($usuario->passUsuario == $datos['Password_Usuario'])
						if (password_verify($usuario->passUsuario, $datos['Password_Usuario']))
						{
							$_SESSION['usuario']['id'] = $datos['Id_Usuario'];
							$_SESSION['usuario']['idEVU'] = $datos['Id_EVU'];
							$_SESSION['usuario']['tipo_Usuario']  = $datos['Id_TU'];
							$_SESSION['usuario']['nombreUsuario'] = $datos['Nombre_Persona'];
							// Enviando respuesta
							echo json_encode(array('error' => false, 'tipo' => $datos['Id_TU'], 'estado' => $datos['Id_EVU']));
						}
						else
						{
							echo json_encode(array('error' => true, 'info' => 'Registro no encontrado.'));
						}
					}
					else
					{
						echo json_encode(array('error' => true));
						$stmt->close();
						$conn->desconectar();
					}
				}
				else
				{
					echo json_encode(array('error' => true, 'info' => $conn->error()));
					$conn->desconectar();
				}
			}
		}
	}
?>