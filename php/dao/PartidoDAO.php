<?php
	require_once('CRUD.php');
	require_once('../ds/DataSource.php');
	require_once('../dto/Partido.php');

	class PartidoDAO implements CRUD
	{
	    function mostrar()
	    {
	    	// Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				echo 'Error al conectar: '.$conn->conectar()->connect_error;
				exit;
			}
			else
			{
				$sql = 'CALL mostrarPartidos()';
				if ($stmt = $conn->preparar($sql))
				{
					$stmt->execute();
					// Asignando a la variable el resultado de la consulta 
					$resultado = $stmt->get_result();
					if ($resultado->num_rows > 0) 
					{
						// Enviando valores obtenidos de la consulta
						foreach ($resultado as $indice => $valor) 
						{
							echo "<tr>
							<td class='align-middle nombrePartido'>".$valor['Nombre_Partido']."</td>
							<td class='align-middle' id='".$valor['Id_Partido']."'>
								<button class='btn btn-info btn-sm editar' data-toggle='modal' data-target='#edit'><li class='fas fa-edit'></li> Editar</button>
								<button class='btn btn-danger btn-sm eliminar' data-toggle='modal' data-target='#del'><li class='fas fa-trash-alt'></li> Eliminar</button>
							</td>
							</tr>";
						}
						// Cerrando conexión
						$stmt->close();
						$conn->desconectar();
					}
					else 
					{
						echo "<tr><td class='align-middle' colspan='2'>No hay datos.</td></tr>";
					}
				}
				else
				{
					// Cerrando conexión
					$stmt->close();
					$conn->desconectar();
					echo 'Error al realizar la consulta: '.$conn->error();
					exit;
				}
			}
	    } // Fin de función mostrar

		function agregar($obj)
		{
			// Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				// Enviando error de conexión
				echo json_encode(array('error' => true, 'info' => $conn->error()));
				exit;
			}
			else
			{
				$partido = $obj;
				$sql = "CALL agregarPartido(?)";
				// Verificando si la consulta se realizó con exito o no
				if ($stmt = $conn->preparar($sql)) 
				{
					// Enviando parámetros a la consulta
					$stmt->bind_param("s", $nombre);
					$nombre = $partido->nombre;
					// Ejecutando consulta
					$stmt->execute();
					// Enviando respuesta
					echo json_encode(array('error' => false, 'info' => 'Registro guardado correctamente'));
				} 
				else 
				{
					// Enviando respuesta
					echo json_encode(array('error' => true, 'info' => $conn->error()));
					exit;
				}
				// Cerrando conexión
				$stmt->close();
				$conn->desconectar();
			}
		} // Fin de función agregar

		function modificar($obj)
		{
			// Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				echo json_encode(array('error' => true, 'info' => $conn->conectar()->connect_error));
				exit;
			}
			else
			{
				$partido = $obj;
				$sql = "CALL editarPartido(?, ?)";
				// Verificando si la consulta se realizó correctamente
				if ($stmt = $conn->preparar($sql))
				{
					// Enviando parámetros a la consulta
					$stmt->bind_param('is', $idPartido, $nombre);
					$idPartido = $partido->idPartido;
					$nombre = $partido->nombre;
					// Ejecutando consulta
					$stmt->execute();
					// Enviando respuesta
					echo json_encode(array('error' => false, 'info' => 'Registro editado correctamente'));
				}
				else
				{
					// Enviando respuesta
					echo json_encode(array('error' => true, 'info' => $conn->error()));
					$stmt->close();
				}
				// Cerrando conexión
				$conn->desconectar();
				exit;
			}
		} // Fin de función modificar

		function eliminar($obj)
		{
			// Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				echo json_encode(array('error' => true, 'info' => $conn->conectar()->connect_error));
				exit;
			}
			else
			{
				$partido = $obj;
				$sql = "CALL eliminarPartido(?)";
				// Verificando si la consulta se realizó correctamente
				if ($stmt = $conn->preparar($sql))
				{
					// Enviando parámetros
					$stmt->bind_param('i', $idPartido);
					$idPartido = $partido->idPartido;
					// Ejecutando consulta
					$stmt->execute();
					// Enviando respuesta
					echo json_encode(array('error' => false, 'info' => 'Registro eliminado correctamente'));
				}
				else
				{
					// Enviando respuesta
					echo json_encode(array('error' => true, 'info' => $conn->error()));
				}
				// Cerrando conexión
				$stmt->close();
				$conn->desconectar();
			}
		} // Fin de función eliminar

		function buscar($val)
		{
			// Instancia para la clase DataSource
			$conn = new DataSource();
			// Verificando estado de la conección
			if (!$conn->conectar())
			{
				echo 'Error al conectar: '.$conn->conectar()->connect_error;
				exit;
			}
			else
			{
				$sql = "SELECT Id_Partido, Nombre_Partido FROM Partido WHERE Nombre_Partido LIKE '%$val%' LIMIT 10";
				if ($stmt = $conn->preparar($sql))
				{
					// Ejecutando la consulta
					$stmt->execute();
					// Obteniendo el resultado de la consulta
					$resultado = $stmt->get_result();
					// Verificando si la consulta devuelve datos
					if ($resultado->num_rows > 0) 
					{
						// Enviando valores obtenidos de la consulta
						foreach ($resultado as $indice => $valor) 
						{
							echo "<tr>
							<td class='align-middle nombrePartido'>".$valor['Nombre_Partido']."</td>
							<td class='align-middle' id='".$valor['Id_Partido']."'>
								<button class='btn btn-info btn-sm editar' data-toggle='modal' data-target='#edit'><li class='fas fa-edit'></li> Editar</button>
								<button class='btn btn-danger btn-sm eliminar' data-toggle='modal' data-target='#del'><li class='fas fa-trash-alt'></li> Eliminar</button>
							</td>
							</tr>";
						}
					}
					else 
					{
						echo "<tr><td class='align-middle' colspan='2'>No hay coincidencias para {$val}.</td></tr>";
					}
					// Cerrando conexión
					$stmt->close();
					$conn->desconectar();
				}
				else
				{
					echo 'Error al realizar la consulta: '.$conn->error();
					exit;
				}
				// Desconectando
				$stmt->close();
				$conn->desconectar();
			}
		} // Fin de función eliminar

		function buscarPersona($val)
		{
			// Nada que hacer
		}

		function llenarSelect1()
		{
			// Nada que hacer
		}

		function llenarSelect2()
		{
			// Nada que hacer
		}

		function verificar($obj)
		{
			// Nada que hacer
		}

	}
?>