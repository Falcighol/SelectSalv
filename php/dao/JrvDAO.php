<?php
require_once 'CRUD.php';
require_once '../ds/DataSource.php';
require_once '../dto/Jrv.php';

/**
 * numero de la clase: JrvDao
 * @author Juan Pablo Elias Hernández
 */
class JrvDao implements CRUD
{
    /**
    * Función para mostrar los datos de la tabla
    */
    public function mostrar()
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conexión
        if (!$conn->conectar()) {
            return false;
            exit;
        } else {
            $jrv = null;
            $jrvs = array();
            $sql = 'CALL mostrarJRV()';
            if ($stmt = $conn->preparar($sql)) {
                $stmt->execute();
                // Asignando a la variable el resultado de la consulta
                $stmt->bind_result($id, $numero, $nombreCV);
                // Enviando valores obtenidos de la consulta
                while ($stmt->fetch()) {
                    $jrv = new Jrv();
                    $jrv->id = (int) $id;
                    $jrv->numero = (int) $numero;
                    $jrv->nombreCV = (string) $nombreCV;
                    array_push($jrvs, (array) $jrv);
                }
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return $jrvs;
                exit;
            } else {
                // Desconectando
                $conn->desconectar();
                // Enviando respuesta
                return null;
                exit;
            }
        }
    }

    /**
    * Función para agregar un registro
    * @param object  Objeto con los datos a agregar
    */
    public function agregar($obj)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar()) {
            return false;
            exit;
        } else {
            $jrv = $obj;
            $sql = "CALL agregarJRV(?, ?)";
            // Verificando si la consulta se realizó con exito o no
            if ($stmt = $conn->preparar($sql)) {
                $stmt->bind_param("ii", $idCV, $numero);
                $idCV = $jrv->idCV;
                $numero = $jrv->numero;
                $stmt->execute();
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return true;
                exit;
            } else {
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return false;
                exit;
            }
        }
    }

    /**
    * Función para modificar un registro
    * @param object Objeto con los datos a modificar
    */
    public function modificar($obj)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar()) {
            return false;
            exit;
        } else {
            $jrv = $obj;
            $sql = "CALL editarJRV(?, ?, ?)";
            // Verificando si la consulta se realizó correctamente
            if ($stmt = $conn->preparar($sql)) {
                $stmt->bind_param('iii', $id, $idCV, $numero);
                $id = $jrv->id;
                $idCV = $jrv->idCV;
                $numero = $jrv->numero;
                $stmt->execute();
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return true;
                exit;
            } else {
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return false;
                exit;
            }
        }
    }

    /**
    * Función para eliminar un registro
    * @param object Objeto con los datos a eliminar
    */
    public function eliminar($obj)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar()) {
            return false;
            exit;
        } else {
            $jrv = $obj;
            $sql = "CALL eliminarJRV(?)";
            // Verificando si la consulta se realizó correctamente
            if ($stmt = $conn->preparar($sql)) {
                // Enviando parámetros
                $stmt->bind_param('i', $id);
                $id = $jrv->id;
                $stmt->execute();
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return true;
            } else {
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return false;
            }
        }
    }

    /**
    * Función para buscar registros
    * @param string Cadena de datos a buscar
    */
    public function buscar($val)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar()) {
            return null;
            exit;
        } else {
            $jrv = null;
            $jrvs = array();
            $sql = "SELECT JRV.Id_JRV, JRV.Numero_JRV, CV.Nombre_CV FROM jrv JRV INNER JOIN centro_votacion CV ON JRV.Id_CV = CV.Id_CV WHERE JRV.Numero_JRV LIKE '%" . $val . "%' OR CV.Nombre_CV LIKE '%" . $val . "%' ORDER BY JRV.Id_JRV DESC LIMIT 20";
            if ($stmt = $conn->preparar($sql)) {
                $stmt->execute();
                // Asignando a la variable el resultado de la consulta
                $stmt->bind_result($id, $numero, $nombreCV);
                // Enviando valores obtenidos de la consulta
                while ($stmt->fetch()) {
                    $jrv = new Jrv();
                    $jrv->id = (int) $id;
                    $jrv->numero = (int) $numero;
                    $jrv->nombreCV = (string) $nombreCV;
                    array_push($jrvs, (array) $jrv);
                }
                //Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return $jrvs;
                exit;
            } else {
                //Desconectando
                $conn->desconectar();
                // Enviando respuesta
                return null;
                exit;
            }
        }
    }

    /**
    * Función para llenar control Select en caso de que se necesite
    */
    public function llenarSelect1()
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar()) {
            echo 'Error al conectar: ' . $conn->conectar()->connect_error;
            exit;
        } else {
            $sql = "SELECT Id_CV, Nombre_CV FROM centro_votacion ORDER BY Nombre_CV ASC;";
            if ($stmt = $conn->preparar($sql)) {
                // Ejecutando la consulta
                $stmt->execute();
                // Obteniendo el resultado de la consulta
                $resultado = $stmt->get_result();
                // Verificando si la consulta devuelve datos
                if ($resultado->num_rows > 0) {
                    // Imprimiendo resultados
                    echo '<option value="0">-Seleccionar-</option>';
                    foreach ($resultado as $indice => $valor) {
                        echo '<option value="' . $valor['Id_CV'] . '">' . $valor['Nombre_CV'] . '</option>';
                    }
                }
            } else {
                $stmt->close();
                $conn->desconectar();
                echo 'Error al realizar la consulta: ' . $conn->error();
                exit;
            }
        }
    }

    public function llenarSelect2()
    {
        // Nada que hacer
    }

    public function verificar($obj)
    {
        // Nada que hacer
    }

    public function buscarPersona($val)
    {
        // Nada que hacer
    }
}
