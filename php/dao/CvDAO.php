<?php
require_once 'CRUD.php';
require_once '../ds/DataSource.php';
require_once '../dto/Cv.php';

/**
* Nombre de la clase: CentroVotacionDAO
* @author Juan Pablo Elias Hernández
*/
class CentroVotacionDAO implements CRUD
{
    public function mostrar()
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar())
        {
            return false;
            exit;
        }
        else
        {
            $centroVotacion = null;
            $centrosVotacion = array();
            $sql = 'CALL mostrarCV()';
            if ($stmt = $conn->preparar($sql))
            {
                $stmt->execute();
                // Asignando a la variable el resultado de la consulta
                $stmt->bind_result($id, $nombre, $direccion, $nombreMunicipio);
                // Enviando valores obtenidos de la consulta
                while ($stmt->fetch())
                {
                    $centroVotacion = new CentroVotacion();
                    $centroVotacion->id = (int) $id;
                    $centroVotacion->nombre = (string) $nombre;
                    $centroVotacion->direccion = (string) $direccion;
                    $centroVotacion->nombreMunicipio = (string) $nombreMunicipio;
                    array_push($centrosVotacion, (array) $centroVotacion);
                }
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return $centrosVotacion;
                exit;
            }
            else
            {
                // Desconectando
                $conn->desconectar();
                // Enviando respuesta
                return null;
                exit;
            }
        }
    }

    public function agregar($obj)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar())
        {
            return false;
            exit;
        }
        else
        {
            $centroVotacion = $obj;
            $sql = "CALL agregarCV(?, ?, ?)";
            // Verificando si la consulta se realizó con exito o no
            if ($stmt = $conn->preparar($sql))
            {
                $stmt->bind_param("iss", $idMunicipio, $nombre, $direccion);
                $idMunicipio = $centroVotacion->idMunicipio;
                $nombre = $centroVotacion->nombre;
                $direccion = $centroVotacion->direccion;
                $stmt->execute();
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return true;
                exit;
            }
            else
            {
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return false;
                exit;
            }
        }
    }

    public function modificar($obj)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar())
        {
            return false;
            exit;
        }
        else
        {
            $centroVotacion = $obj;
            $sql = "CALL editarCV(?, ?, ?, ?)";
            // Verificando si la consulta se realizó correctamente
            if ($stmt = $conn->preparar($sql)) {
                $stmt->bind_param('iiss', $id, $idMunicipio, $nombre, $direccion);
                $id = $centroVotacion->id;
                $idMunicipio = $centroVotacion->idMunicipio;
                $nombre = $centroVotacion->nombre;
                $direccion = $centroVotacion->direccion;
                $stmt->execute();
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return true;
                exit;
            }
            else
            {
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return false;
                exit;
            }
        }
    }

    public function eliminar($obj)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar())
        {
            return false;
            exit;
        }
        else
        {
            $centroVotacion = $obj;
            $sql = "CALL eliminarCV(?)";
            // Verificando si la consulta se realizó correctamente
            if ($stmt = $conn->preparar($sql)) {
                // Enviando parámetros
                $stmt->bind_param('i', $id);
                $id = $centroVotacion->id;
                $stmt->execute();
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return true;
            }
            else
            {
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return false;
            }
        }
    }

    public function buscar($val)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar())
        {
            return null;
            exit;
        }
        else
        {
            $centroVotacion = null;
            $centrosVotacion = array();
            $sql = "SELECT CV.Id_CV, CV.Nombre_CV, CV.Direccion_CV, M.Nombre_Mun FROM centro_votacion CV INNER JOIN municipio M ON CV.Id_Mun = M.Id_Mun WHERE CV.Nombre_CV LIKE '%" . $val . "%' OR CV.Direccion_CV LIKE '%" . $val . "%' OR M.Nombre_Mun = '%" . $val . "%' LIMIT 20";
            if ($stmt = $conn->preparar($sql))
            {
                $stmt->execute();
                // Asignando a la variable el resultado de la consulta
                $stmt->bind_result($id, $nombre, $direccion, $nombreMunicipio);
                // Enviando valores obtenidos de la consulta
                while ($stmt->fetch())
                {
                    $centroVotacion = new CentroVotacion();
                    $centroVotacion->id = (int) $id;
                    $centroVotacion->nombre = (string) $nombre;
                    $centroVotacion->direccion = (string) $direccion;
                    $centroVotacion->nombreMunicipio = (string) $nombreMunicipio;
                    array_push($centrosVotacion, (array) $centroVotacion);
                }
                //Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return $centrosVotacion;
                exit;
            }
            else
            {
                //Desconectando
                $conn->desconectar();
                // Enviando respuesta
                return null;
                exit;
            }
        }
    }

    public function llenarSelect1()
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar())
        {
            echo 'Error al conectar: ' . $conn->conectar()->connect_error;
            exit;
        }
        else
        {
            $sql = "SELECT Id_Mun, Nombre_Mun FROM Municipio ORDER BY Nombre_Mun ASC;";
            if ($stmt = $conn->preparar($sql))
            {
                // Ejecutando la consulta
                $stmt->execute();
                // Obteniendo el resultado de la consulta
                $resultado = $stmt->get_result();
                // Verificando si la consulta devuelve datos
                if ($resultado->num_rows > 0)
                {
                    // Imprimiendo resultados
                    echo '<option value="0">-Seleccionar-</option>';
                    foreach ($resultado as $indice => $valor) {
                        echo '<option value="' . $valor['Id_Mun'] . '">' . $valor['Nombre_Mun'] . '</option>';
                    }
                }
            }
            else
            {
                $stmt->close();
                $conn->desconectar();
                echo 'Error al realizar la consulta: ' . $conn->error();
                exit;
            }
        }
    }

    public function llenarSelect2()
    {
        // Nada que hacer
    }

    public function verificar($obj)
    {
        // Nada que hacer
    }

    public function buscarPersona($val)
    {
        // Nada que hacer
    }
}
