<?php
	/**
	* Interfaz Create Read Update Delete
	* @author Juan Pablo Elias Hernández
	* @version 1.0 
	*/
	interface CRUD
	{
		/**
		* Función para mostrar los datos de la tabla
		*/
		public function mostrar();

		/**
		* Función para agregar un registro
		* @param object  Objeto con los datos a agregar
		*/
		public function agregar($obj);

		/**
		* Función para modificar un registro
		* @param object Objeto con los datos a modificar
		*/
		public function modificar($obj);

		/**
		* Función para eliminar un registro
		* @param object Objeto con los datos a eliminar
		*/
		public function eliminar($obj);

		/**
		* Función para buscar registros
		* @param string Cadena de datos a buscar
		*/
		public function buscar($val);

		/**
		* Función para 
		* @param string Cadena de datos de la persona a buscar
		*/
		public function buscarPersona($val);

		/**
		* Función para llenar control Select en caso de que se necesite
		*/
		public function llenarSelect1();

		/**
		* Función para llenar segundo control Select en caso de que se necesite
		*/
		public function llenarSelect2();

		/**
		* Función para verificar usuario e iniciar una sesión
		* @param object Objeto con datos de usuario a verificar
		*/
		public function verificar($obj);
	}
?>