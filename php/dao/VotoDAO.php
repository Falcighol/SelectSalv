<?php
require_once '../ds/DataSource.php';
require_once '../dto/Jrv.php';
require_once '../dto/Voto.php';

/**
 * numero de la clase: VotoDao
 * @author Juan Pablo Elias Hernández
 */
class VotoDao
{
    /**
    * Función para mostrar los datos de la tabla
    */
    public function seleccionarJRV()
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conexión
        if (!$conn->conectar())
        {
            return null;
            exit;
        }
        else
        {
            $jrv = null;
            $jrvs = array();
            $sql = 'CALL seleccionarJRV()';
            if ($stmt = $conn->preparar($sql))
            {
                $stmt->execute();
                // Asignando a la variable el resultado de la consulta
                $stmt->bind_result($id, $numero, $nombreCV);
                // Enviando valores obtenidos de la consulta
                while ($stmt->fetch())
                {
                    $jrv = new Jrv();
                    $jrv->id = (int) $id;
                    $jrv->numero = (int) $numero;
                    $jrv->nombreCV = (string) $nombreCV;
                    array_push($jrvs, (array) $jrv);
                }
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return $jrvs;
                exit;
            }
            else
            {
                // Desconectando
                $conn->desconectar();
                // Enviando respuesta
                return null;
                exit;
            }
        }
    }

    /**
    * Función para agregar un registro
    * @param object  Objeto con los datos a agregar
    */
    public function agregar($obj)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conección
        if (!$conn->conectar()) {
            return false;
            exit;
        } else {
            $voto = $obj;
            $sql = "CALL agregarVoto(?, ?)";
            // Verificando si la consulta se realizó con exito o no
            if ($stmt = $conn->preparar($sql)) {
                session_start();
                $stmt->bind_param("ii", $idCandidato, $idJRV);
                $idCandidato = $voto->idCandidato;
                $idJRV = $voto->idJRV;
                $stmt->execute();
                $sql = "UPDATE usuario SET Id_EVU = 2 WHERE Id_Usuario = " . $_SESSION['usuario']['id'];
                $stmt = $conn->preparar($sql);
                $stmt->execute();
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return true;
                exit;
            } else {
                // Desconectando
                $stmt->close();
                $conn->desconectar();
                // Enviando respuesta
                return false;
                exit;
            }
        }
    }

    /**
    * Función para llenar chart 
    * @param string Cadena para verificar tipo de chart
    */
    public function chart(String $tipo)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conexión
        if (!$conn->conectar())
        {
            return null;
            exit;
        }
        else
        {
            if ($tipo == 'partido')
            {
                $datos = array(array("Partido", "Votos"));
                $sql = 'CALL chartPartido()';
                $resultado = $conn->conn->query($sql);
                // Enviando valores obtenidos de la consulta
                while ($registro = $resultado->fetch_object())
                {
                    array_push($datos, array($registro->Nombre_Partido, (int) $registro->votos));
                }
                $conn->desconectar();
                // Enviando respuesta
                return $datos;
                exit;
            }
            else if ($tipo = 'departamento')
            {
                $datos = array(array("Departamento", "Votos"));
                $sql = 'CALL chartDepartamento()';
                $resultado = $conn->conn->query($sql);
                // Enviando valores obtenidos de la consulta
                while ($registro = $resultado->fetch_object())
                {
                    array_push($datos, array($registro->nombreDepartamento, (int)$registro->votos));
                }
                $conn->desconectar();
                // Enviando respuesta
                return $datos;
                exit;
            }
        }
    }

    public function reporte(String $tipo)
    {
        // Instancia para la clase DataSource
        $conn = new DataSource();
        // Verificando estado de la conexión
        if (!$conn->conectar())
        {
            return null;
            exit;
        }
        else
        {
            if ($tipo == 'partido') {
                $datos = array();
                $sql = 'CALL reportePartido()';
                $resultado = $conn->conn->query($sql);
                // Enviando valores obtenidos de la consulta
                while ($registro = $resultado->fetch_object())
                {
                    array_push($datos, array($registro->Nombre_Partido, $registro->Nombre_Candidato, (int)$registro->votos));
                }
                $conn->desconectar();
                // Enviando respuesta
                return $datos;
                exit;
            }
        }
    }
}
