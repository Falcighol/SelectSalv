<?php
	// DataSource (DS)
	class DataSource
	{
		private $conn;
		private $host;
		private $usuario;
		private $password;
		private $db;

		public function __set($name, $value)
		{
		    $this->$name = $value;
		}

		public function __get($name)
		{
		    return $this->$name;
		}

	    public function __construct()
	    {
	        $this->host = 'localhost';
	        $this->usuario = 'root';
	        $this->password = '';
	        $this->db = 'selectsalv';
	    }

	    public function conectar()
	    {
	    	$this->conn = new mysqli($this->host, $this->usuario, $this->password, $this->db);
			$this->conn->set_charset('utf8');
	    	if ($this->conn->connect_errno)
	    	{
	    		return false;
	    	} 
	    	else 
	    	{
	    		return true;
	    	}
	    }

	    public function preparar($sql)
	    {
	    	return $this->conn->prepare($sql);
	    }

	    public function desconectar()
	    {
	    	return $this->conn->close();
	    }
	}
?>