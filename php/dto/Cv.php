<?php
	/**
	* Nombre de la clase: CentroVotacion
	* @author Juan Pablo Elias Hernández
	*/
	class CentroVotacion
	{
		private $id, $idMunicipio, $nombre, $direccion, $nombreMunicipio;

		public function __set($name, $value)
		{
			$this->$name = $value; 
		}

		public function __get($name)
		{
			return $this->$name;
		}

		/**
		* Constructor de la clase CentroVotacion 
		*/
		public function __construct()
		{
			$this->id = 0;
			$this->idMunicipio = 0;
			$this->nombreMunicipio = 'Municipio';
			$this->nombre = 'Centro Votacion';
			$this->direccion = 'Direción';
		}
	}
?>