<?php
require_once('../../fpdf/fpdf.php');
require_once('../dao/VotoDAO.php');

class Pdf extends FPDF
{
    function Header()
    {
        //$this->Image('../../img/email-icon.ico', 10, 6);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(276, 5, 'Reporte de votos', 0, 0, 'C');
        $this->Ln();
        $this->SetFont('Times', '', 0);
        $this->Cell(276, 10, 'Reportes por candidato y partido.');
        $this->Ln(20);
    }

    function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial', '', 8);
        $this->Cell(0, 10, 'Página ' . $this->PageNo() . ' /{nb}', 0, 0, 'C');
    }

    function headerTabla()
    {
        $this->SetFont('Times', 'B', 12);
        $this->Cell(60, 10, 'Partido', 1, 0, 'C');
        $this->Cell(60, 10, 'Nombre candidato', 1, 0, 'C');
        $this->Cell(60, 10, 'Votos', 1, 0, 'C');
    }

    function viewTable($data)
    {
        $this->SetFont('Times', '', 12);
        foreach ($data as $datos)
        {
            $this->Cell(60, 20, $datos[0], 1, 0, 'L');
            $this->Cell(60, 20, $datos[1], 1, 0, 'L');
            $this->Cell(60, 20, $datos[2], 1, 0, 'L');
            $this->Ln();
        }
    }
}
$votoDAO = new VotoDao();
$pdf = new Pdf();
$pdf->AliasNbPages();
$pdf->AddPage('L', 'A4', 0);
$pdf->headerTabla();
$pdf->viewTable($votoDAO->reporte('partido'));
$pdf->Output();