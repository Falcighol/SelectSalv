<?php
	class Candidato
	{
		private $idCandidato, $idPartido, $nombreCandidato, $nombrePartido;

		public function __set($name, $value)
		{
			$this->$name = $value; 
		}

		public function __get($name)
		{
			return $this->$name;
		}

		public function __construct()
		{
			$this->idCandidato = 0;
			$this->idPartido = 0;
			$this->nombrePartido = 'Partido';
			$this->nombreCandidato = 'Candidato';
		}
	}
?>