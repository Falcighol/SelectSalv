<?php
	class Partido
	{
		private $idPartido;
		private $nombre;

		public function __set($name, $value)
		{
			$this->$name = $value; 
		}

		public function __get($name)
		{
			return $this->$name;
		}

		public function __construct()
		{
			$this->idPartido = 0;
			$this->nombre = 'N/A';
		}
	}
?>