<?php
	class Usuario
	{
		private $idUsuario;
		private $idTU;
		private $idPersona;
		private $duiPersona;
		private $idEVU;
		private $passUsuario;

		public function __set($name, $value)
		{
			$this->$name = $value; 
		}

		public function __get($name)
		{
			return $this->$name;
		}

		public function __construct()
		{
			$this->idUsuario = 0;
			$this->idTU = 0;
			$this->idPersona = 0;
			$this->duiPersona = '00000000-0';
			$this->idEVU = 0;
			$this->passUsuario = 'password';
		}
	}
?>