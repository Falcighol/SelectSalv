<?php
	require_once('../dao/CandidatoDAO.php');
	require_once('../dao/UsuarioDAO.php');
	require_once('../dao/PartidoDAO.php');
	require_once('../dao/CvDAO.php');
	require_once('../dao/JrvDAO.php');
	require_once('../dao/VotoDAO.php');
	// Instancia a clase CandidatoDAO
	$candidatoDAO = new CandidatoDAO();
	// Instancia a clase UsuarioDAO
	$usuarioDAO = new UsuarioDAO();
	// Instancia a clase PartidoDAO
	$partidoDAO = new PartidoDAO();
	// Instancia a clase CentroVotacionDAO
	$centroVotacionDAO = new CentroVotacionDAO();
	// Instancia a la clase JrvDAO
	$jrvDAO = new JrvDao();
	// Instancia a la clase VotoDao
	$votoDAO = new VotoDao();
	// var_dump($candidatoDAO->mostrar());
	// Verificando si existe la valiable con la operación a realizar
	if (isset($_POST['op']))
	{
		// Verificando si existe el objeto en el que se realizará la operación
		if (isset($_POST['obj']))
		{
			// Igualando valores recibidos a las variables
			$op = $_POST['op'];
			$obj = $_POST['obj'];
			// Verificando la operación a realizar
			if ($op == 'mostrar')
			{
				// Verificando objeto en que se realizará la operación
				if ($obj == 'candidato')
				{
					if ($candidatoDAO->mostrar() != null)
						echo json_encode(array('error' => false, 'datos' => (array) $candidatoDAO->mostrar()));
					else
						echo json_encode(array('error' => true));
				}
				else if ($obj == 'usuario')
				{
					// Ejecutando metodo para mostrar Usuarios
					$usuarioDAO->mostrar();
				}
				else if ($obj == 'partido')
				{
					// Ejecutando metodo para mostrar Partidos
					$partidoDAO->mostrar();
				}
				else if ($obj == 'cv')
				{
					// Ejecutando método para mostrar Centros de Votación
					if ($centroVotacionDAO->mostrar() != null)
						echo json_encode(array('error' => false, 'datos' => (array) $centroVotacionDAO->mostrar()));
					else
						echo json_encode(array('error' => true));
				}
				else if ($obj == 'jrv')
				{
					// Ejecutando método para mostrar Juntas Receptoras de Votos
					if ($jrvDAO->mostrar() != null)
						echo json_encode(array('error' => false, 'datos' => (array) $jrvDAO->mostrar()));
					else
						echo json_encode(array('error' => true));
				}
			}
			else if ($op == 'agregar')
			{
				// Verificando objeto en que se realizará la operación
				if ($obj == 'candidato')
				{
					if (isset($_POST['nombre']) && isset($_POST['partido']))
					{
						$candidato = new Candidato();
						// Llenando objeto con datos para enviarlo
						$candidato->nombreCandidato = $_POST['nombre'];
						$candidato->idPartido = $_POST['partido'];
						// Ejecutando metodo para agregar Candidato
						if ($candidatoDAO->agregar($candidato))
							echo json_encode(array('error' => false, 'info' => 'Registro guardado correctamente.'));
						else
							echo json_encode(array('error' => true, 'info' => 'Error al guardar el registro'));
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
				else if ($obj == 'usuario')
				{
					if (isset($_POST['idPersona']) && isset($_POST['TU']) && isset($_POST['pass']))
					{
						$usuario = new Usuario();
						// Llenando objeto con datos para enviarlo
						$usuario->idPersona = $_POST['idPersona'];
						$usuario->idTU = $_POST['TU'];
						$usuario->passUsuario = $_POST['pass'];
						// Ejecutando metodo para agregar Usuario
						$usuarioDAO->agregar($usuario);
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
				else if ($obj == 'partido')
				{
					if (isset($_POST['nombre']))
					{
						$partido = new Partido();
						// Llenando objeto con datos para enviarlo
						$partido->nombre = $_POST['nombre'];
						// Ejecutando metodo para agregar Partido
						$partidoDAO->agregar($partido);
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
				else if ($obj == 'cv')
				{
					if (isset($_POST['nombre']) && isset($_POST['municipio']) && isset($_POST['direccion']))
					{
						$centroVotacion = new CentroVotacion();
						// Llenando objeto con datos para enviarlo
						$centroVotacion->nombre = $_POST['nombre'];
						$centroVotacion->idMunicipio = $_POST['municipio'];
						$centroVotacion->direccion = $_POST['direccion'];
						// Ejecutando metodo para agregar Centro de Votación
						if ($centroVotacionDAO->agregar($centroVotacion))
							echo json_encode(array('error' => false, 'info' => 'Registro guardado correctamente.'));
						else
							echo json_encode(array('error' => true, 'info' => 'Error al guardar el registro'));
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
				else if ($obj == 'jrv')
				{
					if (isset($_POST['idCV']) && isset($_POST['numero']))
					{
						$jrv = new Jrv();
						// Llenando objeto con datos para enviarlo
						$jrv->idCV = $_POST['idCV'];
						$jrv->numero = $_POST['numero'];
						// Ejecutando metodo para agregar Junta Receptora Votación
						if ($jrvDAO->agregar($jrv))
							echo json_encode(array('error' => false, 'info' => 'Registro guardado correctamente.'));
						else
							echo json_encode(array('error' => true, 'info' => 'Error al guardar el registro'));
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
			}
			else if ($op == 'editar')
			{
				// Verificando objeto en que se realizará la operación
				if ($obj == 'candidato')
				{
					if (isset($_POST['id']) && isset($_POST['nombre']) && isset($_POST['partido']))
					{
						$candidato = new Candidato();
						// Llenando objeto con datos para enviarlo
						$candidato->idCandidato = $_POST['id'];
						$candidato->idPartido = $_POST['partido'];
						$candidato->nombreCandidato = $_POST['nombre'];
						// Ejecutando metodo para modificar Candidatos
						if ($candidatoDAO->modificar($candidato))
							echo json_encode(array('error' => false, 'info' => 'Registro editado correctamente.'));
						else
							echo json_encode(array('error' => true, 'info' => 'Error al editar el registro.'));
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
				else if ($obj == 'usuario')
				{
					if (isset($_POST['idUsuario']) && isset($_POST['TU']) && isset($_POST['EVU']) && isset($_POST['pass']))
					{
						$usuario = new Usuario();
						// Llenando objeto con datos para enviarlo
						$usuario->idUsuario = $_POST['idUsuario'];
						$usuario->idTU = $_POST['TU'];
						$usuario->idEVU = $_POST['EVU'];
						$usuario->passUsuario = $_POST['pass'];
						// Ejecutando metodo para modificar Usuarios
						$usuarioDAO->modificar($usuario);
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
				else if ($obj == 'partido')
				{
					if (isset($_POST['id']) && isset($_POST['nombre']))
					{
						$partido = new Partido();
						// Llenando objeto con datos para enviarlo
						$partido->idPartido = $_POST['id'];
						$partido->nombre = $_POST['nombre'];
						// Ejecutando metodo para modificar Usuarios
						$partidoDAO->modificar($partido);
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
				else if ($obj == 'cv')
				{
					if (isset($_POST['id']) && isset($_POST['nombre']) && isset($_POST['municipio']) && isset($_POST['direccion'])) {
						$centroVotacion = new CentroVotacion();
						// Llenando objeto con datos para enviarlo
						$centroVotacion->id = $_POST['id'];
						$centroVotacion->nombre = $_POST['nombre'];
						$centroVotacion->idMunicipio = $_POST['municipio'];
						$centroVotacion->direccion = $_POST['direccion'];
						// Ejecutando metodo para modificar Centro de Votación
						if ($centroVotacionDAO->modificar($centroVotacion))
							echo json_encode(array('error' => false, 'info' => 'Registro editado correctamente.'));
						else
							echo json_encode(array('error' => true, 'info' => 'Error al editar el registro'));
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
				else if ($obj == 'jrv')
				{
					if (isset($_POST['id']) && isset($_POST['idCV']) && isset($_POST['numero'])) {
						$jrv = new Jrv();
						// Llenando objeto con datos para enviarlo
						$jrv->id = $_POST['id'];
						$jrv->idCV = $_POST['idCV'];
						$jrv->numero = $_POST['numero'];
						// Ejecutando metodo para modificar Junta Receptora de Votos
						if ($jrvDAO->modificar($jrv))
							echo json_encode(array('error' => false, 'info' => 'Registro editado correctamente.'));
						else
							echo json_encode(array('error' => true, 'info' => 'Error al editar el registro'));
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
			}
			else if ($op == 'eliminar')
			{
				// Verificando objeto en que se realizará la operación
				if ($obj == 'candidato')
				{
					if (isset($_POST['id']))
					{
						$id = $_POST['id'];
						// Llenando objeto con datos para enviarlo
						$candidato = new Candidato();
						$candidato->idCandidato = $id;
						// Ejecutando metodo para eliminar Candidato
						if ($candidatoDAO->eliminar($candidato))
							echo json_encode(array('error' => false, 'info' => 'Registro eliminado correctamente.'));
						else
							echo json_encode(array('error' => true, 'info' => 'Error al eliminar el registro.'));
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
				else if ($obj == 'usuario')
				{
					if (isset($_POST['id']))
					{
						$id = $_POST['id'];
						// Llenando objeto con datos para enviarlo
						$usuario = new Usuario();
						$usuario->idUsuario = $id;
						// Ejecutando metodo para eliminar Candidato
						$usuarioDAO->eliminar($usuario);
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
				else if ($obj == 'partido')
				{
					if (isset($_POST['id']))
					{
						$id = $_POST['id'];
						// Llenando objeto con datos para enviarlo
						$partido = new Partido();
						$partido->idPartido = $id;
						// Ejecutando metodo para eliminar Candidato
						$partidoDAO->eliminar($partido);
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
				else if ($obj == 'cv')
				{
					if (isset($_POST['id']))
					{
						$centroVotacion = new CentroVotacion();
						// Llenando objeto con datos para enviarlo
						$centroVotacion->id = $_POST['id'];
						// Ejecutando metodo para eliminar Centro de Votación
						if ($centroVotacionDAO->eliminar($centroVotacion))
							echo json_encode(array('error' => false, 'info' => 'Registro eliminado correctamente.'));
						else
							echo json_encode(array('error' => true, 'info' => 'Error al eliminar el registro'));
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
				else if ($obj == 'jrv')
				{
					if (isset($_POST['id']))
					{
						$jrv = new Jrv();
						// Llenando objeto con datos para enviarlo
						$jrv->id = $_POST['id'];
						// Ejecutando metodo para eliminar Juntas Receptoras de Votos
						if ($jrvDAO->eliminar($jrv))
							echo json_encode(array('error' => false, 'info' => 'Registro eliminado correctamente.'));
						else
							echo json_encode(array('error' => true, 'info' => 'Error al eliminar el registro'));
					}
					else
						// Enviando información del error
						echo json_encode(array('error' => true, 'info' => 'Variable sin datos'));
				}
			}
			else if ($op == 'buscar')
			{
				// Verificando que exista un valor para buscar
				if (isset($_POST['val']))
				{
					$val = $_POST['val'];
					// Verificando objeto en que se realizará la operación
					if ($obj == 'candidato')
					{
						// Ejecutando metodo para buscar Candidatos
						if ($candidatoDAO->buscar($val) != null)
							echo json_encode(array('error' => false, 'datos' => (array) $candidatoDAO->buscar($val)));
						else
							echo json_encode(array('error' => true));
					}
					else if ($obj == 'usuario')
						// Ejecutando metodo para buscar Usuarios
						$usuarioDAO->buscar($val);
					else if ($obj == 'partido')
						// Ejecutando metodo para buscar Usuarios
						$partidoDAO->buscar($val);
					else if ($obj == 'cv')
					{
						// Ejecutando metodo para buscar Centro de Votación
						if ($centroVotacionDAO->buscar($val) != null)
							echo json_encode(array('error' => false, 'datos' => (array) $centroVotacionDAO->buscar($val)));
						else
							echo json_encode(array('error' => true));
					}
					else if ($obj == 'jrv')
					{
						// Ejecutando metodo para buscar Junta Receptora de Votos
						if ($jrvDAO->buscar($val) != null)
							echo json_encode(array('error' => false, 'datos' => (array) $jrvDAO->buscar($val)));
						else
							echo json_encode(array('error' => true));
					}
				}
			}
			else if ($op == 'buscarPersona')
			{
				if ($obj == 'usuario')
				{
					if (isset($_POST['val']))
					{
						$val = $_POST['val'];
						if (empty($val))
							exit;
						// Ejecutando método busar personas
						$usuarioDAO->buscarPersona($val);
					}
				}
			}
			else if ($op == 'llenarSelect')
			{
				// Verificando objeto en que se realizará la operación
				if ($obj == 'candidato')
					// Ejecutando método para llenar select
					$candidatoDAO->llenarSelect1();
				else if ($obj == 'usuario')
					// Ejecutando método para llenar select
					$usuarioDAO->llenarSelect1();
				else if ($obj == 'cv')
					// Ejecutando método para llenar select
					$centroVotacionDAO->llenarSelect1();
				else if ($obj == 'jrv')
					// Ejecutando método para llenar select
					$jrvDAO->llenarSelect1();
			}
			else if ($op == 'llenarSelect2')
			{
				// Verificando objeto en que se realizará la operación
				if ($obj == 'usuario')
					// Ejecutando método para llenar select
					$usuarioDAO->llenarSelect2();
			}
			else if ($op == 'verificar')
			{
				if ($obj == 'usuario')
				{
					if (isset($_POST['dui']) && isset($_POST['password']))
					{
						$usuario = new Usuario();
						// Llenando objeto con datos para enviarlo
						$usuario->duiPersona = $_POST['dui'];
						$usuario->passUsuario = $_POST['password'];
						// Ejecutando método para verificar Usuario
						$usuarioDAO->verificar($usuario);
					}
				}
			}
			else if ($op == 'seleccionarJRV')
			{
				// Ejecutando método para mostrar Juntas Receptoras de Votos
				if ($votoDAO->seleccionarJRV() != null)
					echo json_encode(array('error' => false, 'datos' => (array) $votoDAO->seleccionarJRV()));
				else
					echo json_encode(array('error' => true));
			}
			else if ($op == 'votar')
			{
				$voto = new Voto();
				$voto->idJRV = $_POST['idJRV'];
				$voto->idCandidato = $_POST['idCandidato'];
				if ($votoDAO->agregar($voto))
				{
					if ($_SESSION['usuario']['tipo_Usuario'] == 1)
					{
						echo json_encode(array('error' => false, 'tipo' => 1));
						$_SESSION['usuario']['idEVU'] = 2;
						exit;
					}
					else
					{
						session_destroy();
						echo json_encode(array('error' => false, 'tipo' => 2));
						exit;
					}
				}
				else
					echo json_encode(array('error' => true, 'info' => 'Error al votar'));
			}
			else if ($op == 'chartPartido')
			{
				if ($votoDAO->chart('partido') != null)
					echo json_encode(array('error' => false, 'datos' => $votoDAO->chart('partido')));
				else
					echo json_encode(array('error' => true));
			} 
			else if ($op == 'chartDepartamento')
			{
				if ($votoDAO->chart('departamento') != null)
					echo json_encode(array('error' => false, 'datos' => $votoDAO->chart('departamento')));
				else
					echo json_encode(array('error' => true));
			}
			else if ($op == 'reportePartido')
			{
				if ($votoDAO->reporte('partido') != null)
					echo json_encode(array('error' => false, 'datos' => $votoDAO->reporte('partido')));
				else
					echo json_encode(array('error' => true));
			} // Fin de validación del tipo de operación a realizar
		} // Fin validación de objeto
	} // Fin de validación de operación
?>