<?php
	session_start();
	if (isset($_SESSION['usuario'])) 
	{
		if ($_SESSION['usuario']['tipo_Usuario'] == 1) 
		{
			header('Location: ../admin/');
			exit;
		}
	}
	else
	{
		header('Location: ../login/');
		exit;
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<!-- Bootstrap css -->
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<!-- Icono de la página -->
	<link rel="icon" type="image/ico" href="../img/icon.ico">
	<!-- Scripts -->
	<script type="text/javascript" src="../js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="../js/popper.min.js"></script>
	<script type="text/javascript" src="../js/bootstrap.js"></script>
	<script type="text/javascript" src="../js/fontawesome-all.js"></script>
	<script type="text/javascript" src="../js/votar.js"></script>
	<title>Votar</title>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<h3 class="navbar-brand" style="margin: auto 1rem auto 0;">
			<img src="../img/email-icon.png" width="30" height="30" class="d-inline-block align-top" alt="SelectSalv">
			SelectSalv
		</h3>
	</nav>
	<div class="container">
		<!-- Modal para cofirmación de voto-->
		<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Confirmar Voto</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<span id="pregunta">
						Está a punto de realizar su voto a favor del candidato X.
					</span><br>
					¿Seguro que desea continuar?
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">No estoy seguro</button>
					<button id="btnVotar" type="button" class="btn btn-primary">Sí, estoy seguro</button>
				</div>
				</div>
			</div>
		</div>
		<div class="jumbotron jumbotron-fluid" style="margin-top: 1rem; margin-bottom: 0; padding: 2rem 0;">
			<div class="container">
				<h1 id="brand" style="text-align: center; color: #007BFF; font-size: 6rem; margin-bottom: 2rem;" class="col-12">SELECTSALV</h1>
				<h1 class="display-4">Bienvenido a la página de votación en línea, <?=$_SESSION['usuario']['nombreUsuario']; ?>.</h1>
				<p class="lead" style="margin-top: 1.5rem; font-size: 2rem;">Instrucciones:</p>
				<p class="lead">
					<i class="fa fa-arrow-right" aria-hidden="true"></i> Primero seleciona la Junta Receptora a la que perteneces.
					<div class="form-group col-sm-12 col-md-4">
						<label for="jrv" class="control-label lead">Numero de junta:</label>
						<select name="jrv" class="form-control" id="jrv" autofocus>
							<!-- Opciones de select JRV-->
						</select>
					</div>
				</p>
				<p class="lead">
					<i class="fa fa-arrow-right" aria-hidden="true"></i> Verifica que el número sea el correspondiente.
					<div class="form-group">
						<p class="font-weight-light" style="font-size: 2rem;">
							Junta receptora de votos numero: <span id="numeroJRV" class="font-italic font-weight-normal">Selecione una junta receptora.</span>
						</p>
					</div>
				</p>
				<p class="lead">
					<i class="fa fa-arrow-right" aria-hidden="true"></i> Elige tu candidato favorito y da click en el botón "Votar".<br><br>
					Cada tarjeta contiene informacion acerca de cada candidato y del partido político al que representa.
				</p>
			</div>
			<hr style="margin-top: 3rem;">
		</div>
		<div class="row" id="candidatos" style="display: none;">
		 		<!-- Targetas de candidatos -->
		</div>
	</div>
	<footer style="padding: 1rem;">
		<p style="text-align: center;">
			15 Calle poniente No. 4223, Colonia Escalón
			<br>
			San Salvador, El Salvador, C.A.
			<br>
			Conmutador: (503) 2209-4000
		</p>
		<div style="margin: 1rem auto;" align="center">
			<img src="img/escudo.png" alt="Escudo" style="width: 5rem;">
		</div>
		<p style="text-align: center;">Selectsalv &copy; <?php echo date('Y'); ?> Todos los derechos reservados.</p>
	</footer>
	<style>
	@media (max-width: 720px) {
		#brand {
			display: none;
		}
	}
	</style>
</body>
</html>