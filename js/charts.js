google.charts.load('current', {
	'packages': ['bar']
});

google.charts.setOnLoadCallback(drawChartPar);
google.charts.setOnLoadCallback(drawChartDep);

function drawChartPar() {
	$.post('php/controllers/Controller.php', {
		op: 'chartPartido',
		obj: 'voto'
	},
	function (resp) { 
		var dat = JSON.parse(resp);
		if (!dat.error) {
			var datosArray = new Array();
			dat.datos.forEach(element => {
				datosArray.push(element);
			});
			var data = google.visualization.arrayToDataTable(datosArray);
		
			var options = {
				chart: {
					title: 'Estadísticas por partido.',
					subtitle: 'Cantidad de votos conseguidos por partido hasta el momento.',
				}
			};
		
			var chart = new google.charts.Bar(document.getElementById('chartPar'));
		
			chart.draw(data, google.charts.Bar.convertOptions(options));
		}
	});
}

function drawChartDep() {
	$.post('php/controllers/Controller.php', {
			op: 'chartDepartamento',
			obj: 'voto'
		},
		function (resp) {
			var dat = JSON.parse(resp);
			if (!dat.error) {
				var datosArray = new Array();
				dat.datos.forEach(element => {
					datosArray.push(element);
				});
				var data = google.visualization.arrayToDataTable(datosArray);

				var options = {
					chart: {
						title: 'Estadísticas por departamento.',
						subtitle: 'Cantidad de votos por departamento hasta el momento.',
					}
				};

				var chart = new google.charts.Bar(document.getElementById('chartDep'));

				chart.draw(data, google.charts.Bar.convertOptions(options));
			}
		});
}