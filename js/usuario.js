$(document).ready(function() {
	
	// Llenando Tabla
	llenarTabla();

	// Llenando Select Tipo Usuario
	llenarSelectTU();

	// Llenando select Estado Voto Usuario
	llenarSelectEVU();

	// Variable global para ID a eliminar o Editar
	var idR = 0;

	// Quitando la clase 'is-invalid' de los inputs Agregar en el evento 'Focus'
	$('.frmAgregar .form-group input[type="text"], .frmAgregar .form-group input[type="password"], .frmAgregar .form-group select').on('focus', function() {
		$(this).removeClass('is-invalid');
	});

	// Quitando la clase 'input-error' de los inputs Editar en el evento 'Focus'
	$('.frmEditar .form-group input[type="text"], .frmEditar .form-group input[type="password"], .frmEditar .form-group select').on('focus', function() {
		$(this).removeClass('is-invalid');
	});

	// Función del evento Click del botón Nuevo
	$('#btnNuevo').click(function(e) {
		e.preventDefault();
		// Reset de Selects
		$('#cmbTU').prop('selectedIndex', 0);
		$('#cmbEVU').prop('selectedIndex', 0);
		$('#txtPersona').prop('value', '');
		$('#txtPass').prop('value', '');
		$('#nombrePersona').prop('value', '');
		$('#idPersona').prop('value', '');
		$('.frmAgregar .form-group input[type="text"], .frmAgregar .form-group input[type="password"], .frmAgregar .form-group select').removeClass('is-invalid');
	});
	// Fin de función del evento Click del botón Nuevo

	// Función del evento Click del boton Agregar
	$('#btnAgregar').click(function(e) {
		e.preventDefault();
		// Obteniendo valores del formualario
		var pass = $('#txtPass').val();
		var idP = $('#idPersona').val(), idTU = $('#cmbTU').val();
		// Reemplazando caracteres para evitar errores
		pass.replace("/'/g", "''");
		// Ejecutando método agregar
		agregar(idP, pass, idTU);
	}); // Fin de función del evento Click del botón Agrerar

	// Funcion en el evento KEYUP de la caja de texto 'Buscar'
	$('#txtBuscar').keyup(function(e) {
		e.preventDefault();
		var valor = $('#txtBuscar').val();
		if (valor != "" && valor != " ") {
			// Reemplazando caracteres
			valor.replace("/'/g", "''");
			// Ejecutando método Buscar
			buscar(valor);
		} else {
			// Llenando Tabla
			llenarTabla();
		}
	}); // Fin del evento KEYUP de caja de texto 'Buscar'

	// Funcion en el evento KEYUP de la caja de texto 'Buscar persona'
	$('#txtPersona').keyup(function(e) {
		e.preventDefault();
		var valor = $('#txtPersona').val();
		// Reemplazando caracteres
		valor.replace("/'/g", "''");
		// Ejecutando método Buscar
		if (valor != '') {
			buscarPersona(valor);
		}
		else
		{
			$('#nombrePersona').prop('value', '');
			$('#idPersona').prop('value', '');
		}
	}); // Fin del evento KEYUP de caja de texto 'Buscar persona'

	// Función en el evento Click de cualquier botón 'Eliminar'
	$('body').on('click', '.eliminar', function(e) {
		e.preventDefault();
		// Obteniendo ID del registro a eliminar
		idR = $(this).parent().attr('id');
		var nombreEliminar = $(this).parent().siblings('td.nombrePersona').text();
		// console.log('ID a eliminar: ' + idR);
		$('#pregunta').html('¿Desea eliminar al usuario/a ' + nombreEliminar + '?');
	}); // Fin de función en el evento Click de cualquier botón 'Eliminar'

	// Función en el evento Click del botón ELiminar (Confirmar)
	$('#btnEliminar').click(function(e) {
		e.preventDefault();
		// Ejecutado método Eliminar
		eliminar(idR);
	}); // Fin de evento Click botón Eliminar (Confirmar)

	// Función en el evento Click de cualquier botón 'Editar'
	$('body').on('click', '.editar', function(e) {
		e.preventDefault();
		// Reset de Selects
		$('#cmbEditarTU').prop('selectedIndex', 0);
		$('#cmbEditarEVU').prop('selectedIndex', 0);
		$('#txtEditarPass').prop('value', '');
		$('.frmEditar .form-group input[type="text"], .frmEditar .form-group input[type="password"], .frmEditar .form-group select').removeClass('is-invalid');
		// Obteniendo ID de registro a editar
		idR = $(this).parent().attr('id');
		// Obteniendo datos de la tabla
		var nombreEditar = $(this).parent().siblings('td.nombrePersona').text();
		var TU = $(this).parent().siblings('td.TU').text();
		var EVU = $(this).parent().siblings('td.EVU').text();
		// Comodín para cambiar de Estado o Tipo
		$('span#TU').html('Tipo anterior: ' + TU);
		$('span#EVU').html('Estado anterior: ' + EVU);
		// Enviando datos a inputs
		$('#txtNombreEditar').val(nombreEditar);
		$('#txtIdUsuario').val(idR);
		// console.log('ID a editar: ' + idR);
		// console.log('Nombre: ' + nombreEditar);
	}); // Fin de función en el evento Click de cualquier botón 'Editar'

	// Función en el evento Click del botón Editar (Confirmar)
	$('#btnEditar').click(function(e) {
		e.preventDefault();
		// Obteniendo valores de formulario
		var pass = $('#txtEditarPass').val();
		var TU = $('#cmbEditarTU').val(), EVU = $('#cmbEditarEVU').val();
		// Reemplazando caracteres
		pass.replace("/'/g", "''");
		// Ejecutado método Editar
		editar(idR, TU, EVU, pass);
	}); // Fin de función en el evento Click del botón Editar (Confirmar)

}); // Fin de Document Ready

// Función llenarTabla
function llenarTabla() {
	// Enviando petición Post para llenar Tabla
	$.post('../../../php/controllers/Controller.php', 
		{
			op: 'mostrar', 
			obj: 'usuario'
		}, 
		function(data) {
			$('#cuerpoTablaDatos').html(data).slideDown('slow');
	});
}

// Función llenarSelect
function llenarSelectTU() {
	// Enviando petición Post para llenar Select Partido 
	$.post('../../../php/controllers/Controller.php', 
		{
			op: 'llenarSelect', 
			obj: 'usuario'
		}, 
		function(data) {
		$('#cmbTU').html(data);
		$('#cmbEditarTU').html(data);
	});
} // Fin de llenarSelect

// Función llenarSelect
function llenarSelectEVU() {
	// Enviando petición Post para llenar Select Partido 
	$.post('../../../php/controllers/Controller.php', 
		{
			op: 'llenarSelect2', 
			obj: 'usuario'
		}, 
		function(data) {
		$('#cmbEVU').html(data);
		$('#cmbEditarEVU').html(data);
	});
} // Fin de llenarSelect

// Función validarAgregar
function validarAgregar() {
	var estado = true, tmp;
	// Recorriendo inputs para validarlos
	$('.addRequired').each(function() {
		tmp = $(this).val();
		if (tmp == "" || tmp == " " || tmp == 0) {
			// Agregando clase error
			$(this).addClass('is-invalid');
			estado = false;
		}
	}); // Fin de función Each
	// Retornando estado de los inputs
	return estado;
} // Fin de validarAgregar

function validarEditar() {
	var estado = true, tmp;
	// Recorriendo inputs para validarlos
	$('.editRequired').each(function() {
		tmp = $(this).val();
		if (tmp == "" || tmp == " " || tmp == 0) {
			// Agregando clase error
			$(this).addClass('is-invalid');
			estado = false;
		}
	}); // Fin de función Each
	// Retornando estado de los inputs
	return estado;
} // Fin de validarAgregar

// Función Agregar
function agregar(idP, pass, idTU) {
	// Validando respuesta de la función validarAgregar
	if (validarAgregar()) {
		// Datos a enviar
		var datos = {
			idPersona: idP,
			pass: pass,
			TU: idTU,
			op: 'agregar',
			obj: 'usuario'
		};
		// Realizando petición Ajax
		$.ajax({
			url: '../../../php/controllers/Controller.php',
			type: 'POST',
			data: datos,
			beforeSend: function() {
				// Nada que hacer
			},
			success: function(Respuesta){
				// Convirtiendo datos de la respuesta a JSON
				var resp = JSON.parse(Respuesta);
				// Ocultando ventana modal
				$('#add').modal('hide');
				console.log(resp);
				// Validación de respuesta
				if (!resp.error) {
					// console.log(resp);
					// Mostrando alerta del estado de la consulta
					$('.alert-success').slideDown('slow');
					setTimeout(function(){
						$('.alert-success').slideUp('slow')
					}, 4000);
					$('.alert-success').html(resp.info);
					// Obteniendo datos de la BD para actualizar la tabla
					llenarTabla();
				} else {
					console.log(resp.info);
					// Mostrando alerta del estado de la consulta
					$('.alert-danger').slideDown('slow');
					setTimeout(function(){
						$('.alert-danger').slideUp('slow')
					}, 4000);
					$('.alert-danger').html(resp.info)
				} // Fin de Validación de respuesta
			} // Fin de función Success
		}); // Fin de Ajax
	} else {
		console.log('Incompleto. ' + idP + " " + idTU + " " + pass);
	} // Fin de validación
} // Fin de Agregar 

// Función Editar
function editar(idR, idTU, idEVU, pass) {
	// Validando respuesta de la función validarEditar
	if (validarEditar()) {
		// Datos a enviar
		var datos = {
			idUsuario: idR,
			TU: idTU,
			EVU: idEVU,
			pass: pass,
			op: 'editar',
			obj: 'usuario'
		};
		// Realizando petición Ajax
		$.ajax({
			url: '../../../php/controllers/Controller.php',
			type: 'POST',
			data: datos,
			beforeSend: function() {
				// Nada que hacer
			},
			success: function(Respuesta) {
				// Convirtiendo datos de la respuesta a JSON
				var resp = $.parseJSON(Respuesta);
				console.log(resp);
				// Ocultando ventana modal
				$('#edit').modal('hide');
				// Validación de respuesta
				if (!resp.error) {
					// Mostrando alerta del estado de la consulta
					$('.alert-success').slideDown('slow');
					setTimeout(function(){
						$('.alert-success').slideUp('slow')
					}, 4000);
					$('.alert-success').html(resp.info);
					// Obteniendo datos de la BD para actualizar la tabla
					llenarTabla();
				} else {
					console.log(resp.info);
					// Mostrando alerta del estado de la consulta
					$('.alert-danger').slideDown('slow');
					setTimeout(function(){
						$('.alert-danger').slideUp('slow')
					}, 4000);
					$('.alert-danger').html(resp.info)
				} // Fin de Validación de respuesta
			} // Fin de función Success
		}); // Fin de Ajax
	} // Fin de validación
} // Fin de Editar 

// Función Eliminar
function eliminar(idU) {
	// Ocultando ventana modal
	$('#del').modal('hide');
	// Realizando petición Ajax
	$.ajax({
		url: '../../../php/controllers/Controller.php',
		type: 'POST',
		data: {
			id: idU,
			op: 'eliminar',
			obj: 'usuario'
		},
		beforeSend: function () {
			// Nada que hacer
		},
		success: function (Respuesta) {
			// Convirtiendo respuesta a JSON
			var resp = JSON.parse(Respuesta);
			console.log(resp);
			// Validando respuesta
			if (!resp.error) {
				// Mostrando alerta del estado de la consulta
				$('.alert-success').slideDown('slow');
				setTimeout(function(){
					$('.alert-success').slideUp('slow')
				}, 4000);
				$('.alert-success').html(resp.info);
				// Obteniendo datos de la BD para actualizar la tabla
				llenarTabla();
			} else {
				// Mostrando alerta del estado de la consulta
				$('.alert-danger').slideDown('slow');
				setTimeout(function(){
					$('.alert-danger').slideUp('slow')
				}, 4000);
				$('.alert-danger').html(resp.info);
			} // Fin de validación de respuesa
		} // Fin de función Success
	}); // Fin de Ajax
} // Fin de Eliminar

// Función Buscar
function buscar(valor) {
	// Realizando petición Ajax
	$.ajax({
		url: '../../../php/controllers/Controller.php',
		type: 'POST',
		data: {
			val: valor,
			op: 'buscar',
			obj: 'usuario'
		},
		beforeSend: function(){
			// Nada que hacer
		},
		success: function (result) {
			// Llenando tabla con resultado de la búsqueda
			$('#cuerpoTablaDatos').html(result).slideDown('slow');
		} // Fin de función Success
	}); // Fin de Ajax
} // Fin de Buscar

// Función Buscar Persona
function buscarPersona(valor) {
	// Realizando petición Ajax
	$.ajax({
		url: '../../../php/controllers/Controller.php',
		type: 'POST',
		data: {
			val: valor,
			op: 'buscarPersona',
			obj: 'usuario'
		},
		beforeSend: function(){
			// Nada que hacer
		},
		success: function (Result) {
			var resp = $.parseJSON(Result);
			// Llenando div con resultado de la búsqueda
			if (!resp.error) {
				$('#idPersona').val(resp.id);
				$('#nombrePersona').val(resp.nombre);
			} else if (resp.error) {
				$('#idPersona').val('');
				$('#nombrePersona').val(resp.info);
			}
		} // Fin de función Success
	}); // Fin de Ajax
} // Fin de Buscar Persona
