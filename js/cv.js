$(document).ready(function () {
    // LLenando tabla
    llenarTabla();

    // Llenando control select
    llenarSelect();

    // Variable global para ID a eliminar o Editar
    var idR = 0;

    // Quitando la clase 'is-invalid' de los inputs Agregar en el evento 'Focus'
    $('.frmAgregar .form-group input[type="text"], .frmAgregar .form-group select').on('focus', function () {
        $(this).removeClass('is-invalid');
    });

    // Quitando la clase 'input-error' de los inputs Editar en el evento 'Focus'
    $('.frmEditar .form-group input[type="text"], .frmEditar .form-group select').on('focus', function () {
        $(this).removeClass('is-invalid');
    });

    // Función del evento Click del botón Nuevo
    $('#btnNuevo').click(function (e) {
        e.preventDefault();
        $('#cmbMunicipio').prop('selectedIndex', 0);
        $('#txtNombre').prop('value', '');
        $('#txtDireccion').prop('value', '');
        $('.frmAgregar .form-group input[type="text"], .frmAgregar .form-group select').removeClass('is-invalid');
    });
    // Fin de función del evento Click del botón Nuevo

    // Función del evento Click del boton Agregar
    $('#btnAgregar').click(function (e) {
        e.preventDefault();
        var tmpNom = $('#txtNombre').val(),
            tmpMunicipio = $('#cmbMunicipio').val(),
            tmpDireccion = $('#txtDireccion').val();
        // Reemplazando caracteres para evitar errores
        tmpNom.replace("/'/g", "''");
        tmpMunicipio.replace("/'/g", "''");
        tmpDireccion.replace("/'/g", "''");
        agregar(tmpNom, tmpMunicipio, tmpDireccion);
    }); // Fin de función del evento Click del botón Agrerar

    // Funcion en el evento KEYUP de la caja de texto 'Buscar'
    $('#txtBuscar').keyup(function (e) {
        e.preventDefault();
        var valor = $('#txtBuscar').val();
        valor.replace("/'/g", "''");
        buscar(valor);
    }); // Fin del evento KEYUP de caja de texto 'Buscar'

    // Función en el evento Click de cualquier botón 'Eliminar'
    $('body').on('click', '.eliminar', function (e) {
        e.preventDefault();
        idR = $(this).parent().attr('id');
        var nombreEliminar = $(this).parent().siblings('td.nombreCV').text();
        // console.log('ID a eliminar: ' + idR);
        $('#pregunta').html('¿Desea eliminar el Centro de Votación ' + nombreEliminar + '?');
    }); // Fin de función en el evento Click de cualquier botón 'Eliminar'

    // Función en el evento Click del botón ELiminar (Confirmar)
    $('#btnEliminar').click(function (e) {
        e.preventDefault();
        eliminar(idR);
    }); // Fin de evento Click botón Eliminar (Confirmar)

    // Función en el evento Click de cualquier botón 'Editar'
    $('body').on('click', '.editar', function (e) {
        e.preventDefault();
        $('#cmbEditarMunicipio').prop('selectedIndex', 0);
        $('#txtEditarNombre').prop('value', '');
        $('.frmEditar .form-group input[type="text"], .frmEditar .form-group select').removeClass('is-invalid');
        idR = $(this).parent().attr('id');
        var nombreEditar = $(this).parent().siblings('td.nombreCV').text();
        var nombreMunicipio = $(this).parent().siblings('td.nombreMunicipio').text();
        var direccionEditar = $(this).parent().siblings('td.direccionCV').text();
        $('#txtEditarNombre').val(nombreEditar);
        $('#txtEditarDireccion').val(direccionEditar);
        $('span#nombreMunicipio').html('Municipio anterior: ' + nombreMunicipio);
        $('#txtEditarId').val(idR);
        // console.log('ID a editar: ' + idR);
        // console.log('Nombre: ' + nombreEditar);
    }); // Fin de función en el evento Click de cualquier botón 'Editar'

    // Función en el evento Click del botón Editar (Confirmar)
    $('#btnEditar').click(function (e) {
        e.preventDefault();
        var nom = $('#txtEditarNombre').val(),
            idMunicipio = $('#cmbEditarMunicipio').val(),
            editarDireccion = $('#txtEditarDireccion').val();
        nom.replace("/'/g", "''");
        idMunicipio.replace("/'/g", "''");
        editarDireccion.replace("/'/g", "''");
        editar(idR, nom, idMunicipio, editarDireccion);
    }); // Fin de función en el evento Click del botón Editar (Confirmar)
});

/**
 * Función para llenar tabla con los datos de la BD
 * @author Juan Pablo Elias Hernández
 */
function llenarTabla() {
    // Enviando petición Post para llenar Tabla
    $.post('../../../php/controllers/Controller.php', {
            op: 'mostrar',
            obj: 'cv'
        },
        function (Resp) {
            // Convirtiendo respuesta a JSON
            var res = JSON.parse(Resp);
            // Verificando si no hay error
            if (!res.error) {
                // Inicializando array para almacenar los datos de objeto
                var objArray = new Array();
                // Inicializando contador para recorrer el array
                var i = 0,
                    datosHTML = '';
                // Foreach para recorrer arreglo del objeto 
                res.datos.forEach(element => {
                    // Agregando los datos de objeto al array objArray
                    objArray.push(Object.values((element)));
                    // Concatenando HTML para imprimir datos en la tabla
                    datosHTML += `
						<tr>
                            <td class='align-middle nombreCV'>` + objArray[i][2] + `</td>
                            <td class='align-middle direccionCV'> ` + objArray[i][3] + ` </td>
							<td class='align-middle nombreMunicipio'>` + objArray[i][4] + `</td>
							<td class='align-middle' id='` + objArray[i][0] + `' >
								<button class='btn btn-info btn-sm editar' data-toggle='modal' data-target = '#edit'>
									<i class='fas fa-edit'></i> Editar </button>
								<button class='btn btn-danger btn-sm eliminar' data-toggle='modal' data-target = '#del'>
									<i class='fas fa-trash-alt'></i> Eliminar </button>
							</td>
						</tr>`;
                    i++;
                });
                // Imprimiendo datos
                $('#cuerpoTablaDatos').html(datosHTML);
                $('#tablaDatos').slideDown('slow');
            } else {
                $('#cuerpoTablaDatos').html("<tr><td colspan='4'>No hay datos.</td></tr>").slideDown('slow');
            }
        });
}

/**
 * Función para llenar control select con los datos de la BD
 * @author Juan Pablo Elias Hernández
 */
function llenarSelect() {
    $.post('../../../php/controllers/Controller.php', {
            op: 'llenarSelect',
            obj: 'cv'
        },
        function (data) {
            $('#cmbMunicipio').html(data);
            $('#cmbEditarMunicipio').html(data);
        }
    );
}

// Función validarAgregar
function validarAgregar() {
    var estado = true,
        tmp;
    // Recorriendo inputs para validarlos
    $('.addRequired').each(function () {
        tmp = $(this).val();
        if (tmp == "" || tmp == " " || tmp == 0) {
            // Agregando clase error
            $(this).addClass('is-invalid');
            estado = false;
        }
    }); // Fin de función Each
    // Retornando estado de los inputs
    return estado;
} // Fin de validarAgregar

function validarEditar() {
    var estado = true,
        tmp;
    // Recorriendo inputs para validarlos
    $('.editRequired').each(function () {
        tmp = $(this).val();
        if (tmp == "" || tmp == " " || tmp == 0) {
            // Agregando clase error
            $(this).addClass('is-invalid');
            estado = false;
        }
    }); // Fin de función Each
    // Retornando estado de los inputs
    return estado;
} // Fin de validarAgregar 

/**
 * Función para agregar un registro
 * @author Juan Pablo Elias Hernández
 * @param {*} nomCV Nombre del centro de votación
 * @param {*} idMunicipio  ID del  municipio
 * @param {*} direccion Dirección del CV
 */
function agregar(nomCV, idMunicipio, direccion) {
    // Validando respuesta de la función validarAgregar
    if (validarAgregar()) {
        // Datos a enviar
        var datos = {
            nombre: nomCV,
            municipio: idMunicipio,
            direccion: direccion,
            op: 'agregar',
            obj: 'cv'
        };
        // Realizando petición Ajax
        $.ajax({
            url: '../../../php/controllers/Controller.php',
            type: 'POST',
            data: datos,
            beforeSend: function () {
                // Nada que hacer
            },
            success: function (Respuesta) {
                // Convirtiendo datos de la respuesta a JSON
                var resp = JSON.parse(Respuesta);
                // Ocultando ventana modal
                $('#add').modal('hide');
                // console.log(resp);
                // Validación de respuesta
                if (!resp.error) {
                    $('.alert-success').html(resp.info);
                    // Mostrando alerta del estado de la consulta
                    $('.alert-success').slideDown('slow');
                    setTimeout(function () {
                        $('.alert-success').slideUp('slow')
                    }, 4000);
                    // Obteniendo datos de la BD para actualizar la tabla
                    llenarTabla();
                } else {
                    console.log(resp.info);
                    $('.alert-danger').html(resp.info);
                    // Mostrando alerta del estado de la consulta
                    $('.alert-danger').slideDown('slow');
                    setTimeout(function () {
                        $('.alert-danger').slideUp('slow')
                    }, 4000);
                } // Fin de Validación de respuesta
            } // Fin de función Success
        }); // Fin de Ajax
    } // Fin de validación
} // Fin de Agregar 

/**
 * Función para editar un registro
 * @author Juan Pablo Elias Hernández
 * @param {*} idCV ID del centro de votación
 * @param {*} nomCV Nombre del centro de votación
 * @param {*} idMunicipio ID del municipio
 * @param {*} direccion te dice hacia donde vas
 */
function editar(idCV, nomCV, idMunicipio, direccionCV) {
    // Validando respuesta de la función validarEditar
    if (validarEditar()) {
        // Datos a enviar
        var datos = {
            id: idCV,
            nombre: nomCV,
            municipio: idMunicipio,
            direccion: direccionCV,
            op: 'editar',
            obj: 'cv'
        };
        // Realizando petición Ajax
        $.ajax({
            url: '../../../php/controllers/Controller.php',
            type: 'POST',
            data: datos,
            beforeSend: function () {
                // Nada que hacer
            },
            success: function (Respuesta) {
                // Convirtiendo datos de la respuesta a JSON
                var resp = $.parseJSON(Respuesta);
                // console.log(resp);
                // Ocultando ventana modal
                $('#edit').modal('hide');
                // Validación de respuesta
                if (!resp.error) {
                    $('.alert-success').html(resp.info);
                    // Mostrando alerta del estado de la consulta
                    $('.alert-success').slideDown('slow');
                    setTimeout(function () {
                        $('.alert-success').slideUp('slow')
                    }, 4000);
                    // Obteniendo datos de la BD para actualizar la tabla
                    llenarTabla();
                } else {
                    console.log(resp.info);
                    $('.alert-danger').html(resp.info);
                    // Mostrando alerta del estado de la consulta
                    $('.alert-danger').slideDown('slow');
                    setTimeout(function () {
                        $('.alert-danger').slideUp('slow')
                    }, 4000);
                } // Fin de Validación de respuesta
            } // Fin de función Success
        }); // Fin de Ajax
    } // Fin de validación
} // Fin de Editar 

/**
 * Función para eliminar un registro
 * @author Juan Pablo Elias Hernández 
 * @param {*} id ID del registro
 */
function eliminar(id) {
    // Ocultando ventana modal
    $('#del').modal('hide');
    // Realizando petición Ajax
    $.ajax({
        url: '../../../php/controllers/Controller.php',
        type: 'POST',
        data: {
            id: id,
            op: 'eliminar',
            obj: 'cv'
        },
        beforeSend: function () {
            // Nada que hacer
        },
        success: function (Respuesta) {
            // Convirtiendo respuesta a JSON
            var resp = JSON.parse(Respuesta);
            // console.log(resp);
            // Validando respuesta
            if (!resp.error) {
                $('.alert-success').html(resp.info);
                // Mostrando alerta del estado de la consulta
                $('.alert-success').slideDown('slow');
                setTimeout(function () {
                    $('.alert-success').slideUp('slow')
                }, 4000);
                // Obteniendo datos de la BD para actualizar la tabla
                llenarTabla();
            } else {
                $('.alert-danger').html(resp.info);
                // Mostrando alerta del estado de la consulta
                $('.alert-danger').slideDown('slow');
                setTimeout(function () {
                    $('.alert-danger').slideUp('slow')
                }, 4000);
            } // Fin de validación de respuesa
        } // Fin de función Success
    }); // Fin de Ajax
} // Fin de Eliminar

/**
 * Función para buscar CV
 * @author Juan Pablo Elias Hernaández
 * @param {*} valor
 */
function buscar(valor) {
    // Realizando petición Ajax
    $.ajax({
        url: '../../../php/controllers/Controller.php',
        type: 'POST',
        data: {
            val: valor,
            op: 'buscar',
            obj: 'cv'
        },
        beforeSend: function () {
            // Nada que hacer
        },
        success: function (result) {
            // Llenando tabla con resultado de la búsqueda
            var res = JSON.parse(result);
            if (!res.error) {
                // Inicializando array para almacenar los datos de objeto
                var objArray = new Array();
                // Inicializando contador para recorrer el array
                var i = 0,
                    datosHTML = '';
                // Foreach para recorrer arreglo del objeto 
                res.datos.forEach(element => {
                    // Agregando los datos de objeto al array objArray
                    objArray.push(Object.values((element)));
                    // Concatenando HTML para imprimir datos en la tabla
                    datosHTML += `
						<tr>
                            <td class='align-middle nombreCV'>` + objArray[i][2] + `</td>
                            <td class='align-middle direccionCV'> ` + objArray[i][3] + ` </td>
							<td class='align-middle nombreMunicipio'>` + objArray[i][4] + `</td>
							<td class='align-middle' id='` + objArray[i][0] + `' >
								<button class='btn btn-info btn-sm editar' data-toggle='modal' data-target = '#edit'>
									<i class='fas fa-edit'></i> Editar </button>
								<button class='btn btn-danger btn-sm eliminar' data-toggle='modal' data-target = '#del'>
									<i class='fas fa-trash-alt'></i> Eliminar </button>
							</td>
						</tr>`;
                    i++;
                });
                // Imprimiendo datos
                $('#cuerpoTablaDatos').html(datosHTML).slideDown('slow');
            } else
                $('#cuerpoTablaDatos').html("<tr><td colspan='4'>No hay coincidencias para '" + valor + "'.</td></tr>").slideDown('slow');
        } // Fin de función Success
    }); // Fin de Ajax
} // Fin de Buscar

