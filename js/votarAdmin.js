$(document).ready(function () {
	// Quitando clases de estado
	$('#jrv').removeClass('is-valid');
	$('#jrv').removeClass('is-invalid');

	// Declarando variable global
	var idCandidato = 0;

	// Llenando tarjetas con los candidatos
	llenarTarjetas();

	// Llenando control selecto con juntas receptoras(
	llenarSelect();

	// Imprimiendo JRV selecionada
	$('#jrv').change(function () {
		validarJRV();
	});

	// Mostrando modal
	$('body').on('click', '.btn-votar', function (e) {
		e.preventDefault();
		var candidato = $(this).siblings('h5.card-title').text();
		var partido = $(this).siblings('p.card-text').text();
		idCandidato = $(this).attr('id');
		$('#pregunta').html('Está apunto de realizar su voto a favor del candidato <b>' + candidato + '</b>, que representa al partido <b>' + partido + '</b>');
	});

	// Ejecuntando método votar
	$('body').on('click', '#btnVotar', function (e) {
		e.preventDefault();
		var idJRV = $('#jrv').val();
		votar(idJRV, idCandidato);
	});
});

/**
 * Función para llenar tarjetas
 */
function llenarTarjetas() {
	// Enviando petición Post para llenar Tabla
	$.post('../../../php/controllers/Controller.php', {
			op: 'mostrar',
			obj: 'candidato'
		},
		function (Resp) {
			var res = JSON.parse(Resp);
			if (!res.error) {
				var objArray = new Array();
				var i = 0,
					datosHTML = '';
				res.datos.forEach(element => {
					objArray.push(Object.values((element)));
					datosHTML += `
						<div class="col-auto col-md" style="margin: auto;" >
							<div class="card" style="width: 18rem; color: #252525; background-color: #fff; margin: 0 auto 2rem auto;">
								<img class="card-img-top" src="../../../img/candidato.png" alt="Card image cap">
								<div class="card-body">
									<h5 class="card-title">` + objArray[i][2] + `</h5>
									<p class="card-text"> ` + objArray[i][3] + ` </p>
									<button type="button" class="btn btn-outline-info btn-votar" id="` + objArray[i][0] + `" style="width: 100%" data-toggle="modal" data-target="#confirm" disabled> Votar </button>
								</div>
							</div>
						</div>`;
					i++;
				});
				$('#candidatos').html(datosHTML);
				$('#candidatos').slideDown('slow');
			}
		});
}

/**
 * Función llenar select
 */
function llenarSelect() {
	$.post('../../../php/controllers/Controller.php', {
			op: 'seleccionarJRV',
			obj: 'votar'
		},
		function (data) {
			// Convirtiendo respuesta a JSON
			var res = JSON.parse(data);
			// Verificando si no hay error
			if (!res.error) {
				// Inicializando array para almacenar los datos de objeto
				var objArray = new Array();
				// Inicializando contador para recorrer el array
				var i = 0,
					datosHTML = '<option value="0">-Seleccionar-</option>';
				// Foreach para recorrer arreglo del objeto 
				res.datos.forEach(element => {
					// Agregando los datos de objeto al array objArray
					objArray.push(Object.values((element)));
					// Concatenando HTML para imprimir datos en la tabla
					datosHTML += "<option value='" + objArray[i][0] + "'>" + objArray[i][3] + " - " + objArray[i][2] + "</option>";
					i++;
				});
				// Imprimiendo datos
				$('#jrv').html(datosHTML);
			} else {
				$('#jrv').html("<option value='0'>-No hay datos.-</option>");
			}
		}
	);
}

/**
 * Función para validar el voto
 */
function validarJRV() {
	var estado = true;
	if ($('#jrv').val() == 0) {
		$('#jrv').removeClass('is-valid');
		// Agregando clase error
		$('#jrv').addClass('is-invalid');
		$('#numeroJRV').text('Selecione una junta receptora.');
		$('.btn-votar').attr("disabled", true);
		estado = false;
	} else {
		$('#jrv').removeClass('is-invalid');
		$('#jrv').addClass('is-valid');
		$('#numeroJRV').text($('#jrv option:selected').text());
		$('.btn-votar').attr("disabled", false);
	}
	// Retornando estado del control
	return estado;
}

/**
 * Método para votar
 * @param {*} idJRV ID de junta receptora
 * @param {*} idCandidato ID de candidato
 */
function votar(idJRV, idCandidato) {
	if (validarJRV())
	{
		$.post('../../../php/controllers/Controller.php', {
				op: 'votar',
				obj: 'voto',
				idJRV: idJRV,
				idCandidato: idCandidato
			},
			function (data) {
				var resp = JSON.parse(data);
				if (!resp.error) {
					if (resp.tipo == 1)
						location.href = '../../';
					else if (resp.tipo == 2)
						location.href = '../../../login/';
				}
		});
	}
}