$(document).ready(function() {
	
	// Llenando Tabla
	llenarTabla();

	// Variable global para ID a eliminar o Editar
	var idR = 0;

	// Quitando la clase 'is-invalid' de los inputs Agregar en el evento 'Focus'
	$('.frmAgregar .form-group input[type="text"], .frmAgregar .form-group select').on('focus', function() {
		$(this).removeClass('is-invalid');
	});

	// Quitando la clase 'input-error' de los inputs Editar en el evento 'Focus'
	$('.frmEditar .form-group input[type="text"], .frmEditar .form-group select').on('focus', function() {
		$(this).removeClass('is-invalid');
	});

	// Función del evento Click del botón Nuevo
	$('#btnNuevo').click(function(e) {
		e.preventDefault();
		$('#txtNombre').prop('value', '');
		$('.frmAgregar .form-group input[type="text"], .frmAgregar .form-group select').removeClass('is-invalid');
	});
	// Fin de función del evento Click del botón Nuevo

	// Función del evento Click del boton Agregar
	$('#btnAgregar').click(function(e) {
		e.preventDefault();
		var tmpNom = $('#txtNombre').val();
		// Reemplazando caracteres para evitar errores
		tmpNom.replace("/'/g", "''");
		// Ejecutando método agregar
		agregar(tmpNom);
	}); // Fin de función del evento Click del botón Agrerar

	// Funcion en el evento KEYUP de la caja de texto 'Buscar'
	$('#txtBuscar').keyup(function(e) {
		e.preventDefault();
		var valor = $('#txtBuscar').val();
		// Validando que el valor no sea nulo
		if (valor != "" && valor != " ") {
			// Reemplazando caracteres
			valor.replace("/'/g", "''");
			// Ejecutando método buscar
			buscar(valor);
		} else {
			// Llenando tabla
			llenarTabla();
		}
	}); // Fin del evento KEYUP de caja de texto 'Buscar'

	// Función en el evento Click de cualquier botón 'Eliminar'
	$('body').on('click', '.eliminar', function(e) {
		e.preventDefault();
		// Obteniendo ID de partido a eliminar
		idR = $(this).parent().attr('id');
		// Obteniendo nombre de partido a eliminar
		var nombreEliminar = $(this).parent().siblings('td.nombrePartido').text();
		// console.log('ID a eliminar: ' + idR);
		$('#pregunta').html('¿Desea eliminar al partido ' + nombreEliminar + '?');
	}); // Fin de función en el evento Click de cualquier botón 'Eliminar'

	// Función en el evento Click del botón ELiminar (Confirmar)
	$('#btnEliminar').click(function(e) {
		e.preventDefault();
		// Ejecutando método eliminar
		eliminar(idR);
	}); // Fin de evento Click botón Eliminar (Confirmar)

	// Función en el evento Click de cualquier botón 'Editar'
	$('body').on('click', '.editar', function(e) {
		e.preventDefault();
		// Limpiando inputs
		$('#txtEditarNombre').prop('value', '');
		$('.frmEditar .form-group input[type="text"], .frmEditar .form-group select').removeClass('is-invalid');
		// Obteniendo ID de partido a editar
		idR = $(this).parent().attr('id');
		// Obteniendo nombre de partido a editar
		var nombreEditar = $(this).parent().siblings('td.nombrePartido').text();
		// Enviando nombre obtenido al input EditarNombre
		$('#txtEditarNombre').val(nombreEditar);
		// Enviando ID obtenido al input EditarId
		$('#txtEditarId').val(idR);
		// console.log('ID a editar: ' + idR);
		// console.log('Nombre: ' + nombreEditar);
	}); // Fin de función en el evento Click de cualquier botón 'Editar'

	// Función en el evento Click del botón Editar (Confirmar)
	$('#btnEditar').click(function(e) {
		e.preventDefault();
		// Obteniendo valor de input EditarNombre
		var nom = $('#txtEditarNombre').val();
		// Remplazando caracteres
		nom.replace("/'/g", "''");
		// Ejecutando método editar
		editar(idR, nom);
	}); // Fin de función en el evento Click del botón Editar (Confirmar)

}); // Fin de Document Ready

// Función llenarTabla
function llenarTabla() {
	// Enviando petición Post para llenar Tabla
	$.post('../../../php/controllers/Controller.php', 
		{
			op: 'mostrar',
			obj: 'partido'
		}, 
		function(data) {
			// Llenando tabla desde la base de datos
			$('#cuerpoTablaDatos').html(data).slideDown('slow');
	});
}

// Función validarAgregar
function validarAgregar() {
	var estado = true, tmp;
	// Recorriendo inputs para validarlos
	$('.addRequired').each(function() {
		tmp = $(this).val();
		if (tmp == "" || tmp == " " || tmp == 0) {
			// Agregando clase error
			$(this).addClass('is-invalid');
			estado = false;
		}
	}); // Fin de función Each
	// Retornando estado de los inputs
	return estado;
} // Fin de validarAgregar

function validarEditar() {
	var estado = true, tmp;
	// Recorriendo inputs para validarlos
	$('.editRequired').each(function() {
		tmp = $(this).val();
		if (tmp == "" || tmp == " " || tmp == 0) {
			// Agregando clase error
			$(this).addClass('is-invalid');
			estado = false;
		}
	}); // Fin de función Each
	// Retornando estado de los inputs
	return estado;
} // Fin de validarAgregar 

// Función Agregar
function agregar(nomPartido) {
	// Validando respuesta de la función validarAgregar
	if (validarAgregar()) {
		// Datos a enviar
		var datos = {
			nombre: nomPartido,
			op: 'agregar',
			obj: 'partido'
		};
		// Realizando petición Ajax
		$.ajax({
			url: '../../../php/controllers/Controller.php',
			type: 'POST',
			data: datos,
			beforeSend: function() {
				// Nada que hacer
			},
			success: function(Respuesta){
				// Convirtiendo datos de la respuesta a JSON
				var resp = JSON.parse(Respuesta);
				// Ocultando ventana modal
				$('#add').modal('hide');
				console.log(resp);
				// Validación de respuesta
				if (!resp.error) {
					// console.log(resp);
					// Mostrando alerta del estado de la consulta
					$('.alert-success').slideDown('slow');
					setTimeout(function(){
						$('.alert-success').slideUp('slow')
					}, 4000);
					$('.alert-success').html(resp.info);
					// Obteniendo datos de la BD para actualizar la tabla
					llenarTabla();
				} else {
					console.log(resp.info);
					// Mostrando alerta del estado de la consulta
					$('.alert-danger').slideDown('slow');
					setTimeout(function(){
						$('.alert-danger').slideUp('slow')
					}, 4000);
					$('.alert-danger').html(resp.info)
				} // Fin de Validación de respuesta
			} // Fin de función Success
		}); // Fin de Ajax
	} // Fin de validación
} // Fin de Agregar 

// Función Editar
function editar(idPartido, nomPartido) {
	// Validando respuesta de la función validarEditar
	if (validarEditar()) {
		// Datos a enviar
		var datos = {
			id: idPartido,
			nombre: nomPartido,
			op: 'editar',
			obj: 'partido'
		};
		// Realizando petición Ajax
		$.ajax({
			url: '../../../php/controllers/Controller.php',
			type: 'POST',
			data: datos,
			beforeSend: function() {
				// Nada que hacer
			},
			success: function(Respuesta) {
				// Convirtiendo datos de la respuesta a JSON
				var resp = JSON.parse(Respuesta);
				console.log(resp);
				// Ocultando ventana modal
				$('#edit').modal('hide');
				// Validación de respuesta
				if (!resp.error) {
					// Mostrando alerta del estado de la consulta
					$('.alert-success').slideDown('slow');
					setTimeout(function(){
						$('.alert-success').slideUp('slow')
					}, 4000);
					$('.alert-success').html(resp.info);
					// Obteniendo datos de la BD para actualizar la tabla
					llenarTabla();
				} else {
					console.log(resp.info);
					// Mostrando alerta del estado de la consulta
					$('.alert-danger').slideDown('slow');
					setTimeout(function(){
						$('.alert-danger').slideUp('slow')
					}, 4000);
					$('.alert-danger').html(resp.info)
				} // Fin de Validación de respuesta
			} // Fin de función Success
		}); // Fin de Ajax
	} // Fin de validación
} // Fin de Editar 

// Función Eliminar
function eliminar(idPartido) {
	// Ocultando ventana modal
	$('#del').modal('hide');
	// Realizando petición Ajax
	$.ajax({
		url: '../../../php/controllers/Controller.php',
		type: 'POST',
		data: {
			id: idPartido,
			op: 'eliminar',
			obj: 'partido'
		},
		beforeSend: function () {
			// Nada que hacer
		},
		success: function (Respuesta) {
			// Convirtiendo respuesta a JSON
			var resp = JSON.parse(Respuesta);
			console.log(resp);
			// Validando respuesta
			if (!resp.error) {
				// Mostrando alerta del estado de la consulta
				$('.alert-success').slideDown('slow');
				setTimeout(function(){
					$('.alert-success').slideUp('slow')
				}, 4000);
				$('.alert-success').html(resp.info);
				// Obteniendo datos de la BD para actualizar la tabla
				llenarTabla();
			} else {
				// Mostrando alerta del estado de la consulta
				$('.alert-danger').slideDown('slow');
				setTimeout(function(){
					$('.alert-danger').slideUp('slow')
				}, 4000);
				$('.alert-danger').html(resp.info);
			} // Fin de validación de respuesa
		} // Fin de función Success
	}); // Fin de Ajax
} // Fin de Eliminar

// Función Buscar
function buscar(valor) {
	// Realizando petición Ajax
	$.ajax({
		url: '../../../php/controllers/Controller.php',
		type: 'POST',
		data: {
			val: valor,
			op: 'buscar',
			obj: 'partido'
		},
		beforeSend: function(){
			// Nada que hacer
		},
		success: function (result) {
			// Llenando tabla con resultado de la búsqueda
			$('#cuerpoTablaDatos').html(result).slideDown('slow');
		} // Fin de función Success
	}); // Fin de Ajax
} // Fin de Buscar
