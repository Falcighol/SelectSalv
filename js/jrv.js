$(document).ready(function () {
    // LLenando tabla
    llenarTabla();

    // Llenando control select
    llenarSelect();

    // Variable global para ID a eliminar o Editar
    var idR = 0;

    // Quitando la clase 'is-invalid' de los inputs Agregar en el evento 'Focus'
    $('.frmAgregar .form-group input[type="text"], .frmAgregar .form-group select').on('focus', function () {
        $(this).removeClass('is-invalid');
    });

    // Quitando la clase 'input-error' de los inputs Editar en el evento 'Focus'
    $('.frmEditar .form-group input[type="text"], .frmEditar .form-group select').on('focus', function () {
        $(this).removeClass('is-invalid');
    });

    // Función del evento Click del botón Nuevo
    $('#btnNuevo').click(function (e) {
        e.preventDefault();
        $('#cmbCV').prop('selectedIndex', 0);
        $('#txtNumero').prop('value', '');
        $('.frmAgregar .form-group input[type="text"], .frmAgregar .form-group select').removeClass('is-invalid');
    });
    // Fin de función del evento Click del botón Nuevo

    // Función del evento Click del boton Agregar
    $('#btnAgregar').click(function (e) {
        e.preventDefault();
        var tmpNum = $('#txtNumero').val(),
            tmpCV = $('#cmbCV').val();
        // Reemplazando caracteres para evitar errores
        tmpNum.replace("/'/g", "''");
        tmpCV.replace("/'/g", "''");
        agregar(tmpNum, tmpCV);
    }); // Fin de función del evento Click del botón Agrerar

    // Funcion en el evento KEYUP de la caja de texto 'Buscar'
    $('#txtBuscar').keyup(function (e) {
        e.preventDefault();
        var valor = $('#txtBuscar').val();
        valor.replace("/'/g", "''");
        buscar(valor);
    }); // Fin del evento KEYUP de caja de texto 'Buscar'

    // Función en el evento Click de cualquier botón 'Eliminar'
    $('body').on('click', '.eliminar', function (e) {
        e.preventDefault();
        idR = $(this).parent().attr('id');
        var numeroEliminar = $(this).parent().siblings('td.numeroJRV').text();
        // console.log('ID a eliminar: ' + idR);
        $('#pregunta').html('¿Desea eliminar junta receptora numero ' + numeroEliminar + '?');
    }); // Fin de función en el evento Click de cualquier botón 'Eliminar'

    // Función en el evento Click del botón ELiminar (Confirmar)
    $('#btnEliminar').click(function (e) {
        e.preventDefault();
        eliminar(idR);
    }); // Fin de evento Click botón Eliminar (Confirmar)

    // Función en el evento Click de cualquier botón 'Editar'
    $('body').on('click', '.editar', function (e) {
        e.preventDefault();
        $('#cmbEditarCV').prop('selectedIndex', 0);
        $('#txtEditarNumero').prop('value', '');
        $('.frmEditar .form-group input[type="text"], .frmEditar .form-group select').removeClass('is-invalid');
        idR = $(this).parent().attr('id');
        var numeroEditar = $(this).parent().siblings('td.numeroJRV').text();
        var nombreCV = $(this).parent().siblings('td.nombreCV').text();
        $('#txtEditarNumero').val(numeroEditar);
        $('span#nombreCV').html('Centro de votación anterior: ' + nombreCV);
        $('#txtEditarId').val(idR);
    }); // Fin de función en el evento Click de cualquier botón 'Editar'

    // Función en el evento Click del botón Editar (Confirmar)
    $('#btnEditar').click(function (e) {
        e.preventDefault();
        var num = $('#txtEditarNumero').val(),
            idCV = $('#cmbEditarCV').val();
        num.replace("/'/g", "''");
        idCV.replace("/'/g", "''");
        editar(idR, num, idCV);
    }); // Fin de función en el evento Click del botón Editar (Confirmar)
});

/**
 * Función para llenar tabla con los datos de la BD
 * @author Juan Pablo Elias Hernández
 */
function llenarTabla() {
    // Enviando petición Post para llenar Tabla
    $.post('../../../php/controllers/Controller.php', {
            op: 'mostrar',
            obj: 'jrv'
        },
        function (Resp) {
            // Convirtiendo respuesta a JSON
            var res = JSON.parse(Resp);
            // Verificando si no hay error
            if (!res.error) {
                // Inicializando array para almacenar los datos de objeto
                var objArray = new Array();
                // Inicializando contador para recorrer el array
                var i = 0,
                    datosHTML = '';
                // Foreach para recorrer arreglo del objeto 
                res.datos.forEach(element => {
                    // Agregando los datos de objeto al array objArray
                    objArray.push(Object.values((element)));
                    // Concatenando HTML para imprimir datos en la tabla
                    datosHTML += `
						<tr>
                            <td class='align-middle numeroJRV'>` + objArray[i][3] + `</td>
                            <td class='align-middle nombreCV'> ` + objArray[i][2] + ` </td>
							<td class='align-middle' id='` + objArray[i][0] + `' >
								<button class='btn btn-info btn-sm editar' data-toggle='modal' data-target = '#edit'>
									<i class='fas fa-edit'></i> Editar </button>
								<button class='btn btn-danger btn-sm eliminar' data-toggle='modal' data-target = '#del'>
									<i class='fas fa-trash-alt'></i> Eliminar </button>
							</td>
						</tr>`;
                    i++;
                });
                // Imprimiendo datos
                $('#cuerpoTablaDatos').html(datosHTML);
                $('#tablaDatos').slideDown('slow');
            } else {
                $('#cuerpoTablaDatos').html("<tr><td colspan='4'>No hay datos.</td></tr>").slideDown('slow');
            }
        });
}

/**
 * Función para llenar control select con los datos de la BD
 * @author Juan Pablo Elias Hernández
 */
function llenarSelect() {
    $.post('../../../php/controllers/Controller.php', {
            op: 'llenarSelect',
            obj: 'jrv'
        },
        function (data) {
            $('#cmbCV').html(data);
            $('#cmbEditarCV').html(data);
        }
    );
}

// Función validarAgregar
function validarAgregar() {
    var estado = true,
        tmp;
    // Recorriendo inputs para validarlos
    $('.addRequired').each(function () {
        tmp = $(this).val();
        if (tmp == "" || tmp == " " || tmp == 0) {
            // Agregando clase error
            $(this).addClass('is-invalid');
            estado = false;
        }
    }); // Fin de función Each
    // Retornando estado de los inputs
    return estado;
} // Fin de validarAgregar

function validarEditar() {
    var estado = true,
        tmp;
    // Recorriendo inputs para validarlos
    $('.editRequired').each(function () {
        tmp = $(this).val();
        if (tmp == "" || tmp == " " || tmp == 0) {
            // Agregando clase error
            $(this).addClass('is-invalid');
            estado = false;
        }
    }); // Fin de función Each
    // Retornando estado de los inputs
    return estado;
} // Fin de validarAgregar 

/**
 * Función para agregar un registro
 * @author Juan Pablo Elias Hernández
 * @param {*} numero Número de la junta receptora de votos
 * @param {*} idCV  ID del centro de votación
 */
function agregar(numero, idCV) {
    // Validando respuesta de la función validarAgregar
    if (validarAgregar()) {
        // Datos a enviar
        var datos = {
            numero: numero,
            idCV: idCV,
            op: 'agregar',
            obj: 'jrv'
        };
        // Realizando petición Ajax
        $.ajax({
            url: '../../../php/controllers/Controller.php',
            type: 'POST',
            data: datos,
            beforeSend: function () {
                // Nada que hacer
            },
            success: function (Respuesta) {
                // Convirtiendo datos de la respuesta a JSON
                var resp = JSON.parse(Respuesta);
                // Ocultando ventana modal
                $('#add').modal('hide');
                // console.log(resp);
                // Validación de respuesta
                if (!resp.error) {
                    $('.alert-success').html(resp.info);
                    // Mostrando alerta del estado de la consulta
                    $('.alert-success').slideDown('slow');
                    setTimeout(function () {
                        $('.alert-success').slideUp('slow')
                    }, 4000);
                    // Obteniendo datos de la BD para actualizar la tabla
                    llenarTabla();
                } else {
                    console.log(resp.info);
                    $('.alert-danger').html(resp.info);
                    // Mostrando alerta del estado de la consulta
                    $('.alert-danger').slideDown('slow');
                    setTimeout(function () {
                        $('.alert-danger').slideUp('slow')
                    }, 4000);
                } // Fin de Validación de respuesta
            } // Fin de función Success
        }); // Fin de Ajax
    } // Fin de validación
} // Fin de Agregar 

/**
 * Función para editar un registro
 * @author Juan Pablo Elias Hernández
 * @param {*} id ID de la junta receptora de votos
 * @param {*} numero Numero del centro de votación
 * @param {*} idCV ID del centro de votación
 */
function editar(id, numero, idCV) {
    // Validando respuesta de la función validarEditar
    if (validarEditar()) {
        // Datos a enviar
        var datos = {
            id: id,
            numero: numero,
            idCV: idCV,
            op: 'editar',
            obj: 'jrv'
        };
        // Realizando petición Ajax
        $.ajax({
            url: '../../../php/controllers/Controller.php',
            type: 'POST',
            data: datos,
            beforeSend: function () {
                // Nada que hacer
            },
            success: function (Respuesta) {
                // Convirtiendo datos de la respuesta a JSON
                var resp = $.parseJSON(Respuesta);
                // console.log(resp);
                // Ocultando ventana modal
                $('#edit').modal('hide');
                // Validación de respuesta
                if (!resp.error) {
                    $('.alert-success').html(resp.info);
                    // Mostrando alerta del estado de la consulta
                    $('.alert-success').slideDown('slow');
                    setTimeout(function () {
                        $('.alert-success').slideUp('slow')
                    }, 4000);
                    // Obteniendo datos de la BD para actualizar la tabla
                    llenarTabla();
                } else {
                    console.log(resp.info);
                    $('.alert-danger').html(resp.info);
                    // Mostrando alerta del estado de la consulta
                    $('.alert-danger').slideDown('slow');
                    setTimeout(function () {
                        $('.alert-danger').slideUp('slow')
                    }, 4000);
                } // Fin de Validación de respuesta
            } // Fin de función Success
        }); // Fin de Ajax
    } // Fin de validación
} // Fin de Editar 

/**
 * Función para eliminar un registro
 * @author Juan Pablo Elias Hernández 
 * @param {*} id ID del registro
 */
function eliminar(id) {
    // Ocultando ventana modal
    $('#del').modal('hide');
    // Realizando petición Ajax
    $.ajax({
        url: '../../../php/controllers/Controller.php',
        type: 'POST',
        data: {
            id: id,
            op: 'eliminar',
            obj: 'jrv'
        },
        beforeSend: function () {
            // Nada que hacer
        },
        success: function (Respuesta) {
            // Convirtiendo respuesta a JSON
            var resp = JSON.parse(Respuesta);
            // console.log(resp);
            // Validando respuesta
            if (!resp.error) {
                $('.alert-success').html(resp.info);
                // Mostrando alerta del estado de la consulta
                $('.alert-success').slideDown('slow');
                setTimeout(function () {
                    $('.alert-success').slideUp('slow')
                }, 4000);
                // Obteniendo datos de la BD para actualizar la tabla
                llenarTabla();
            } else {
                $('.alert-danger').html(resp.info);
                // Mostrando alerta del estado de la consulta
                $('.alert-danger').slideDown('slow');
                setTimeout(function () {
                    $('.alert-danger').slideUp('slow')
                }, 4000);
            } // Fin de validación de respuesa
        } // Fin de función Success
    }); // Fin de Ajax
} // Fin de Eliminar

/**
 * Función para buscar CV
 * @author Juan Pablo Elias Hernaández
 * @param {*} valor
 */
function buscar(valor) {
    // Realizando petición Ajax
    $.ajax({
        url: '../../../php/controllers/Controller.php',
        type: 'POST',
        data: {
            val: valor,
            op: 'buscar',
            obj: 'jrv'
        },
        beforeSend: function () {
            // Nada que hacer
        },
        success: function (result) {
            // Llenando tabla con resultado de la búsqueda
            var res = JSON.parse(result);
            if (!res.error) {
                // Inicializando array para almacenar los datos de objeto
                var objArray = new Array();
                // Inicializando contador para recorrer el array
                var i = 0,
                    datosHTML = '';
                // Foreach para recorrer arreglo del objeto 
                res.datos.forEach(element => {
                    // Agregando los datos de objeto al array objArray
                    objArray.push(Object.values((element)));
                    // Concatenando HTML para imprimir datos en la tabla
                    datosHTML += `
						<tr>
                            <td class='align-middle numeroJRV'>` + objArray[i][3] + `</td>
                            <td class='align-middle nombreCV'> ` + objArray[i][2] + ` </td>
							<td class='align-middle' id='` + objArray[i][0] + `' >
								<button class='btn btn-info btn-sm editar' data-toggle='modal' data-target = '#edit'>
									<i class='fas fa-edit'></i> Editar </button>
								<button class='btn btn-danger btn-sm eliminar' data-toggle='modal' data-target = '#del'>
									<i class='fas fa-trash-alt'></i> Eliminar </button>
							</td>
						</tr>`;
                    i++;
                });
                // Imprimiendo datos
                $('#cuerpoTablaDatos').html(datosHTML).slideDown('slow');
            } else
                $('#cuerpoTablaDatos').html("<tr><td colspan='3'>No hay coincidencias para '" + valor + "'.</td></tr>").slideDown('slow');
        } // Fin de función Success
    }); // Fin de Ajax
} // Fin de Buscar
