$(document).ready(function () {

	// Llenando Tabla
	llenarTabla();

	// Llenando Control Select
	llenarSelect();

	// Variable global para ID a eliminar o Editar
	var idR = 0;

	// Quitando la clase 'is-invalid' de los inputs Agregar en el evento 'Focus'
	$('.frmAgregar .form-group input[type="text"], .frmAgregar .form-group select').on('focus', function () {
		$(this).removeClass('is-invalid');
	});

	// Quitando la clase 'input-error' de los inputs Editar en el evento 'Focus'
	$('.frmEditar .form-group input[type="text"], .frmEditar .form-group select').on('focus', function () {
		$(this).removeClass('is-invalid');
	});

	// Función del evento Click del botón Nuevo
	$('#btnNuevo').click(function (e) {
		e.preventDefault();
		$('#cmbPartido').prop('selectedIndex', 0);
		$('#txtNombre').prop('value', '');
		$('.frmAgregar .form-group input[type="text"], .frmAgregar .form-group select').removeClass('is-invalid');
	});
	// Fin de función del evento Click del botón Nuevo

	// Función del evento Click del boton Agregar
	$('#btnAgregar').click(function (e) {
		e.preventDefault();
		var tmpNom = $('#txtNombre').val(),
			tmpPartido = $('#cmbPartido').val();
		// Reemplazando caracteres para evitar errores
		tmpNom.replace("/'/g", "''");
		tmpPartido.replace("/'/g", "''");
		agregar(tmpNom, tmpPartido);
	}); // Fin de función del evento Click del botón Agrerar

	// Funcion en el evento KEYUP de la caja de texto 'Buscar'
	$('#txtBuscar').keyup(function (e) {
		e.preventDefault();
		var valor = $('#txtBuscar').val();
		valor.replace("/'/g", "''");
		buscar(valor);
	}); // Fin del evento KEYUP de caja de texto 'Buscar'

	// Función en el evento Click de cualquier botón 'Eliminar'
	$('body').on('click', '.eliminar', function (e) {
		e.preventDefault();
		idR = $(this).parent().attr('id');
		var nombreEliminar = $(this).parent().siblings('td.nombreCandidato').text();
		// console.log('ID a eliminar: ' + idR);
		$('#pregunta').html('¿Desea eliminar al candidato/a ' + nombreEliminar + '?');
	}); // Fin de función en el evento Click de cualquier botón 'Eliminar'

	// Función en el evento Click del botón ELiminar (Confirmar)
	$('#btnEliminar').click(function (e) {
		e.preventDefault();
		eliminar(idR);
	}); // Fin de evento Click botón Eliminar (Confirmar)

	// Función en el evento Click de cualquier botón 'Editar'
	$('body').on('click', '.editar', function (e) {
		e.preventDefault();
		$('#cmbEditarPartido').prop('selectedIndex', 0);
		$('#txtEditarNombre').prop('value', '');
		$('.frmEditar .form-group input[type="text"], .frmEditar .form-group select').removeClass('is-invalid');
		idR = $(this).parent().attr('id');
		var nombreEditar = $(this).parent().siblings('td.nombreCandidato').text();
		var nombrePartido = $(this).parent().siblings('td.nombrePartido').text();
		$('#txtEditarNombre').val(nombreEditar);
		$('span#nombrePartido').html('Partido anterior: ' + nombrePartido);
		$('#txtEditarId').val(idR);
		// console.log('ID a editar: ' + idR);
		// console.log('Nombre: ' + nombreEditar);
	}); // Fin de función en el evento Click de cualquier botón 'Editar'

	// Función en el evento Click del botón Editar (Confirmar)
	$('#btnEditar').click(function (e) {
		e.preventDefault();
		var nom = $('#txtEditarNombre').val(),
			idPartido = $('#cmbEditarPartido').val();
		nom.replace("/'/g", "''");
		idPartido.replace("/'/g", "''");
		editar(idR, nom, idPartido);
	}); // Fin de función en el evento Click del botón Editar (Confirmar)

}); // Fin de Document Ready

// Función llenarTabla
function llenarTabla() {
	// Enviando petición Post para llenar Tabla
	$.post('../../../php/controllers/Controller.php', {
			op: 'mostrar',
			obj: 'candidato'
		},
		function (Resp) {
			var res = JSON.parse(Resp);
			if (!res.error) {
				var objArray = new Array();
				var i = 0,
					datosHTML = '';
				res.datos.forEach(element => {
					objArray.push(Object.values((element)));
					datosHTML += `
						<tr>
							<td class='align-middle nombreCandidato'>` + objArray[i][2] + `</td>
							<td class='align-middle nombrePartido'>` + objArray[i][3] + `</td>
							<td class='align-middle' id='` + objArray[i][0] + `' >
								<button class='btn btn-info btn-sm editar' data-toggle='modal' data-target = '#edit'>
									<li class='fas fa-edit'> </li> Editar </button>
								<button class='btn btn-danger btn-sm eliminar' data-toggle='modal' data-target = '#del'>
									<li class='fas fa-trash-alt' > </li> Eliminar </button>
							</td>
						</tr>`;
					i++;
				});
				$('#cuerpoTablaDatos').html(datosHTML).slideDown('slow');
			} else {
				$('#cuerpoTablaDatos').html("<tr><td colspan='4'>No hay datos.</td></tr>").slideDown('slow');
			}
		});
}

// Función llenarSelect
function llenarSelect() {
	// Enviando petición Post para llenar Select Partido 
	$.post('../../../php/controllers/Controller.php', {
			op: 'llenarSelect',
			obj: 'candidato'
		},
		function (data) {
			$('#cmbPartido').html(data);
			$('#cmbEditarPartido').html(data);
		});
} // Fin de llenarSelect

// Función validarAgregar
function validarAgregar() {
	var estado = true,
		tmp;
	// Recorriendo inputs para validarlos
	$('.addRequired').each(function () {
		tmp = $(this).val();
		if (tmp == "" || tmp == " " || tmp == 0) {
			// Agregando clase error
			$(this).addClass('is-invalid');
			estado = false;
		}
	}); // Fin de función Each
	// Retornando estado de los inputs
	return estado;
} // Fin de validarAgregar

function validarEditar() {
	var estado = true,
		tmp;
	// Recorriendo inputs para validarlos
	$('.editRequired').each(function () {
		tmp = $(this).val();
		if (tmp == "" || tmp == " " || tmp == 0) {
			// Agregando clase error
			$(this).addClass('is-invalid');
			estado = false;
		}
	}); // Fin de función Each
	// Retornando estado de los inputs
	return estado;
} // Fin de validarAgregar 

// Función Agregar
function agregar(nomCandidato, idPartido) {
	// Validando respuesta de la función validarAgregar
	if (validarAgregar()) {
		// Datos a enviar
		var datos = {
			nombre: nomCandidato,
			partido: idPartido,
			op: 'agregar',
			obj: 'candidato'
		};
		// Realizando petición Ajax
		$.ajax({
			url: '../../../php/controllers/Controller.php',
			type: 'POST',
			data: datos,
			beforeSend: function () {
				// Nada que hacer
			},
			success: function (Respuesta) {
				// Convirtiendo datos de la respuesta a JSON
				var resp = JSON.parse(Respuesta);
				// Ocultando ventana modal
				$('#add').modal('hide');
				// console.log(resp);
				// Validación de respuesta
				if (!resp.error) {
					$('.alert-success').html(resp.info);
					// Mostrando alerta del estado de la consulta
					$('.alert-success').slideDown('slow');
					setTimeout(function () {
						$('.alert-success').slideUp('slow')
					}, 4000);
					// Obteniendo datos de la BD para actualizar la tabla
					llenarTabla();
				} else {
					console.log(resp.info);
					$('.alert-danger').html(resp.info);
					// Mostrando alerta del estado de la consulta
					$('.alert-danger').slideDown('slow');
					setTimeout(function () {
						$('.alert-danger').slideUp('slow')
					}, 4000);
				} // Fin de Validación de respuesta
			} // Fin de función Success
		}); // Fin de Ajax
	} // Fin de validación
} // Fin de Agregar 

// Función Editar
function editar(idCandidato, nomCandidato, idPartido) {
	// Validando respuesta de la función validarEditar
	if (validarEditar()) {
		// Datos a enviar
		var datos = {
			id: idCandidato,
			nombre: nomCandidato,
			partido: idPartido,
			op: 'editar',
			obj: 'candidato'
		};
		// Realizando petición Ajax
		$.ajax({
			url: '../../../php/controllers/Controller.php',
			type: 'POST',
			data: datos,
			beforeSend: function () {
				// Nada que hacer
			},
			success: function (Respuesta) {
				// Convirtiendo datos de la respuesta a JSON
				var resp = $.parseJSON(Respuesta);
				// console.log(resp);
				// Ocultando ventana modal
				$('#edit').modal('hide');
				// Validación de respuesta
				if (!resp.error) {
					$('.alert-success').html(resp.info);
					// Mostrando alerta del estado de la consulta
					$('.alert-success').slideDown('slow');
					setTimeout(function () {
						$('.alert-success').slideUp('slow')
					}, 4000);
					// Obteniendo datos de la BD para actualizar la tabla
					llenarTabla();
				} else {
					console.log(resp.info);
					$('.alert-danger').html(resp.info);
					// Mostrando alerta del estado de la consulta
					$('.alert-danger').slideDown('slow');
					setTimeout(function () {
						$('.alert-danger').slideUp('slow')
					}, 4000);
				} // Fin de Validación de respuesta
			} // Fin de función Success
		}); // Fin de Ajax
	} // Fin de validación
} // Fin de Editar 

// Función Eliminar
function eliminar(id) {
	// Ocultando ventana modal
	$('#del').modal('hide');
	// Realizando petición Ajax
	$.ajax({
		url: '../../../php/controllers/Controller.php',
		type: 'POST',
		data: {
			id: id,
			op: 'eliminar',
			obj: 'candidato'
		},
		beforeSend: function () {
			// Nada que hacer
		},
		success: function (Respuesta) {
			// Convirtiendo respuesta a JSON
			var resp = JSON.parse(Respuesta);
			// console.log(resp);
			// Validando respuesta
			if (!resp.error) {
				$('.alert-success').html(resp.info);
				// Mostrando alerta del estado de la consulta
				$('.alert-success').slideDown('slow');
				setTimeout(function () {
					$('.alert-success').slideUp('slow')
				}, 4000);
				// Obteniendo datos de la BD para actualizar la tabla
				llenarTabla();
			} else {
				$('.alert-danger').html(resp.info);
				// Mostrando alerta del estado de la consulta
				$('.alert-danger').slideDown('slow');
				setTimeout(function () {
					$('.alert-danger').slideUp('slow')
				}, 4000);
			} // Fin de validación de respuesa
		} // Fin de función Success
	}); // Fin de Ajax
} // Fin de Eliminar

// Función Buscar
function buscar(valor) {
	// Realizando petición Ajax
	$.ajax({
		url: '../../../php/controllers/Controller.php',
		type: 'POST',
		data: {
			val: valor,
			op: 'buscar',
			obj: 'candidato'
		},
		beforeSend: function () {
			// Nada que hacer
		},
		success: function (result) {
			// Llenando tabla con resultado de la búsqueda
			var res = JSON.parse(result);
			if (!res.error) {
				var objArray = new Array();
				var i = 0,
					datosHTML = '';
				res.datos.forEach(element => {
					objArray.push(Object.values((element)));
					datosHTML += `
						<tr>
							<td class='align-middle nombreCandidato'>` + objArray[i][2] + `</td>
							<td class='align-middle nombrePartido'>` + objArray[i][3] + `</td>
							<td class='align-middle' id='` + objArray[i][0] + `' >
								<button class='btn btn-info btn-sm editar' data-toggle='modal' data-target = '#edit'>
									<li class='fas fa-edit'> </li> Editar </button>
								<button class='btn btn-danger btn-sm eliminar' data-toggle='modal' data-target = '#del'>
									<li class='fas fa-trash-alt' > </li> Eliminar </button>
							</td>
						</tr>`;
					i++;
				});
				$('#cuerpoTablaDatos').html(datosHTML).slideDown('slow');
			} else {
				$('#cuerpoTablaDatos').html("<tr><td colspan='4'>No hay coincedencias para " + valor + ".</td></tr>").slideDown('slow');
			}
		} // Fin de función Success
	}); // Fin de Ajax
} // Fin de Buscar