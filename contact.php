<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<!-- Bootstrap css -->
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/style.css">
	<!-- Icono de la página -->
	<link rel="icon" type="image/ico" href="img/icon.ico">
	<!-- Scripts -->
	<script src="js/jquery-3.2.1.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/fontawesome-all.js"></script>
	<title>Contáctanos</title>
</head>
<body>
	<!-- Menu -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<!-- Brand -->
		<a href="index.php" class="navbar-brand" style="margin: auto 1rem auto 0;">
			<img src="img/email-icon.png" width="30" class="d-inline-block align-top" alt="SelectSalv">
			SelectSalv
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent" style="text-align: center;">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="index.php"><span class="fas fa-home"></span> Inicio</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="charts.php"><span class="fas fa-chart-line"></span> Estadísticas</a>
				</li>
				<li class="nav-item active">
					<a class="nav-link" href="contact.php"><span class="fas fa-phone"></span> Contáctanos</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="about.php"><span class="fas fa-info"></span> Sobre nosotros</a>
				</li>
			</ul>
			<div class="form-inline my-2 my-lg-0" align='center'>
				<a class="btn btn-outline-light my-2 my-sm-0" href="login/" style="margin: auto;"><i class="fas fa-sign-in-alt"></i> Votar / Iniciar Sesión</a>
			</div>
		</div>
	</nav>
	<div class="container">
		<div class="jumbotron bg-light text-center" style="margin-top: 3rem;">
			<h1 class="display-4">Tribunal Supremo Electoral</h1>
			<p class="lead">
				República de El Salvador, Centroamerica<br>
				15 calle poniente #4223, Colonia Escalón, San Salvador<br>
				PBX: (503) 2209-4000
			</p>
			<div class="table-responsive-md">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15504.518074896629!2d-89.2372789!3d13.7106045!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f63301a1d532811%3A0x85ae833cfee19a60!2s15+Calle+Poniente%2C+San+Salvador!5e0!3m2!1sen!2ssv!4v1527543414368" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
		<footer style="padding: 1rem;">
			<p style="text-align: center;">
				15 Calle poniente No. 4223, Colonia Escalón
				<br>
				San Salvador, El Salvador, C.A.
				<br>
				Conmutador: (503) 2209-4000
			</p>
			<div style="margin: 1rem auto;" align="center">
				<img src="img/escudo.png" alt="Escudo" style="width: 5rem;">
			</div>
			<p style="text-align: center;">Selectsalv &copy; <?php echo date('Y'); ?> Todos los derechos reservados.</p>
		</footer>
	</div>
</body>
</html>