<?php
	session_start();
	if (isset($_SESSION['usuario'])) 
	{
		if ($_SESSION['usuario']['tipo_Usuario'] == 2) 
		{
			header('Location: ../votar/');
		}
		else if ($_SESSION['usuario']['tipo_Usuario'] == 1)
		{
			header('Location: ../admin/');
		}
	}
	else
	{
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<!-- Bootstrap css -->
	<link rel="stylesheet" href="../css/bootstrap.css">
	<link rel="stylesheet" href="../css/login.css">
	<!-- Icono de la página -->
	<link rel="icon" type="image/ico" href="../img/icon.ico">
	<!-- Scripts -->
	<script src="../js/jquery-3.2.1.min.js"></script>
	<script src="../js/popper.min.js"></script>
	<script src="../js/bootstrap.js"></script>
	<script src="../js/fontawesome-all.js"></script>
	<script src="../js/login.js"></script>
	<title>Iniciar Sesión</title>
</head>
<body>
	<!-- Menu -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<!-- Brand -->
		<a href="../" class="navbar-brand" style="margin: auto 1rem auto 0;">
			<img src="../img/email-icon.png" width="30" class="d-inline-block align-top" alt="SelectSalv">
			SelectSalv
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent" style="text-align: center;">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="../"><span class="fas fa-home"></span> Inicio</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../charts.php"><span class="fas fa-chart-line"></span> Estadísticas</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../contact.php"><span class="fas fa-phone"></span> Contáctanos</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="../about.php"><span class="fas fa-info"></span> Sobre nosotros</a>
				</li>
			</ul>
			<!--
			<div class="form-inline my-2 my-lg-0" align='center'>
				<a class="btn btn-outline-dark my-2 my-sm-0" style="margin: auto;" href="#">Iniciar sessión</a>
			</div> -->
		</div>
	</nav>
	<div class="container">
		<div class="row">
			<div class="jumbotron jumbotron-fluid col-sm-12 col-md-7 col-lg-7 col-xl-7" style="margin: 0;">
				<div class="container">
					<h1 class="display-4">Iniciar sesión</h1>
					<p class="lead">
						Ingrese su DUI y contraseña para acceder al sistema. <br>
						Recuerde que antes debe registrarse con su DUI y establecer una contraseña.
					</p>
					<img src="../img/democracy.jpg" alt="Democracia" width="100%">
				</div>
			</div>
			<div class="jumbotron jumbotron-fluid col-sm-12 col-md-5 col-lg-5 col-xl-5" style="margin: auto 0;">
				<div class="container" style="background-color: #fff; padding: 3rem 1rem;">
					<form class="col-12 login-form" align="center">
						<img src="../img/lock-icon.png" class="mb-4" style="margin-bottom: 1.5rem;" width="30%">
						<br>
					  	<div class="form-group" align='left'>
					   		<label for="txtUsuario">Ingrese su DUI:</label>
					    	<input type="text" class="form-control form-control-lg required" id="txtUsuario" placeholder="DUI">
					  	</div>
					  	<div class="form-group" align='left'>
					    	<label for="txtPass">Ingrese su contraseña:</label>
					    	<input type="password" class="form-control form-control-lg required" id="txtPass" placeholder="Contraseña" re>
					  	</div>
					  	<button type="button" class="btn btn-info col-12" style="margin-top: 0.7rem; padding: .5rem 0; font-size: 1.5rem;" id="btnLogin">
					  		Iniciar Sesión
					  	</button><style type="text/css">
							.alert { display: none; }
						</style>
						<div class="alert alert-danger" role="alert" style="margin-top: 1.5rem;">
							<!--Información del error-->
						</div>
					</form>
				</div>
			</div>
		</div>
		<footer style="padding: 1rem;">
			<p style="text-align: center;">
                15 Calle poniente No. 4223, Colonia Escalón
                <br>
                San Salvador, El Salvador, C.A.
                <br>
                Conmutador: (503) 2209-4000
			</p>
            <div style="margin: 1rem auto;" align="center">
                <img src="../img/escudo.png" alt="Escudo" style="width: 5rem;">
            </div>
            <p style="text-align: center;">Selectsalv &copy; <?php echo date('Y'); ?> Todos los derechos reservados.</p>
		</footer>
	</div>
</body>
</html>
<?php } ?>